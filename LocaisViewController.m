//
//  LocaisViewController.m
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "LocaisViewController.h"

@interface LocaisViewController ()

@end

@implementation LocaisViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _local.name;
}
@end
