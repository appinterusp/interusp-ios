//
//  LoadingView.m
//  Swap
//
//  Created by AppSimples on 3/18/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(LoadingView*)addLoadingViewToView:(UIView*)parentView{
    NSArray * viewArray = [[NSBundle mainBundle] loadNibNamed:@"LoadingView" owner:nil options:nil];
    
    if ([viewArray count] != 1) {
        return nil;
    }
    
    LoadingView * instance = (LoadingView*)[viewArray firstObject];
    if (instance) {
        instance.frame = CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height);
        [parentView addSubview:instance];
    }
    return instance;
}

@end
