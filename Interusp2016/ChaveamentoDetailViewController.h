//
//  ChaveamentoDetailViewController.h
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChaveamentoDetailViewController : UIViewController

+(ChaveamentoDetailViewController*)initWithModalidade:(NSString*)modalidadeString andCode:(int)modalidadeCode;

@end
