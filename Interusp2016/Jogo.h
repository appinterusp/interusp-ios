//
//  Jogo.h
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Jogo : NSObject

@property (nonatomic, strong) NSString * horario;
@property (nonatomic, strong) NSDate * data;
@property (nonatomic, strong) NSString * dataString;
@property (nonatomic, strong) NSString * local;
@property (nonatomic, strong) NSArray * participantes;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * subtitle;
@property (nonatomic, strong) NSString * dia;

@property BOOL isProva;

//trocar por model?
@property (nonatomic, strong) NSString * modalidade;
@property int modalidadeCode;

+(Jogo*)initWithDictionary:(NSDictionary*)dictionary;
+(Jogo*)initWithServiceResultDict:(NSDictionary*)dictionary;

@end
