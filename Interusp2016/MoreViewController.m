//
//  MoreViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "MoreViewController.h"
#import "SimpleCell.h"
#import "ScoreGeralViewController.h"
#import "TeamSelectionViewController.h"
#import "UserConfig.h"
#import "GritosViewController.h"
#import "HistoricoViewController.h"
#import "InfoViewController.h"
#import "AboutViewController.h"
#import "TorcidometroViewController.h"

@interface MoreViewController ()

@property (nonatomic, strong) NSArray * viewList;
@property (nonatomic, weak) IBOutlet UITableView * table;

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.viewList = @[@"InterUsp",@"Pontuação",@"Torcidômetro",@"Selecionar Atlética", @"Gritos", @"Histórico" , @"Sobre"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.viewList objectAtIndex:indexPath.row];
    
    SimpleCell * cell = [self.table dequeueReusableCellWithIdentifier:@"SIMPLECELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTitle:viewString];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.viewList objectAtIndex:indexPath.row];
    
    if ([viewString isEqualToString:@"InterUsp"]) {
        InfoViewController * vc = [[InfoViewController alloc] initWithNibName:@"InfoViewController" bundle:nil];
        vc.title = @"InterUsp";
        [self.navigationController pushViewController:vc animated:true];
    }
    
    if ([viewString isEqualToString:@"Sobre"]) {
        AboutViewController * vc = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
        vc.title = @"Sobre";
        [self.navigationController pushViewController:vc animated:true];
    }
    
    if ([viewString isEqualToString:@"Torcidômetro"]) {
        TorcidometroViewController * vc = [[TorcidometroViewController alloc] initWithNibName:@"TorcidometroViewController" bundle:nil];
        vc.title = @"Torcidômetro";
        [self.navigationController pushViewController:vc animated:true];
    }
    
    if ([viewString isEqualToString:@"Pontuação"]) {
        ScoreGeralViewController * vc = [[ScoreGeralViewController alloc] initWithNibName:@"ScoreGeralViewController" bundle:nil];
        vc.title = @"Pontuação";
        [self.navigationController pushViewController:vc animated:true];
    }
    
    if ([viewString isEqualToString:@"Selecionar Atlética"]) {
        [[UserConfig shared] selectTeam:IUTeamNotSelected];
        [self.tabBarController dismissViewControllerAnimated:true completion:^{}];
        //[self.tabBarController removeFromParentViewController];
    }
    
    if ([viewString isEqualToString:@"Gritos"]) {
        GritosViewController * vc = [[GritosViewController alloc] initWithNibName:@"GritosViewController" bundle:nil];
        vc.title = @"Gritos";
        [self.navigationController pushViewController:vc animated:true];
    }
    
    if ([viewString isEqualToString:@"Histórico"]) {
        HistoricoViewController * vc = [[HistoricoViewController alloc] initWithNibName:@"HistoricoViewController" bundle:nil];
        vc.title = @"Histórico";
        [self.navigationController pushViewController:vc animated:true];
    }
}

@end
