//
//  UserConfig.m
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "UserConfig.h"

@implementation UserConfig

#define kColorPoli2 [UIColor colorWithRed:243/255.0 green:251/255.0 blue:5/255.0 alpha:1.0]
#define kColorPoli1 [UIColor colorWithRed:23/255.0 green:27/255.0 blue:64/255.0 alpha:1.0]
#define kColorRibeirao1 [UIColor colorWithRed:9/255.0 green:59/255.0 blue:36/255.0 alpha:1.0]
#define kColorRibeirao2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorFarma1 [UIColor colorWithRed:130/255.0 green:21/255.0 blue:24/255.0 alpha:1.0]
#define kColorFarma2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorFea1 [UIColor colorWithRed:0/255.0 green:0/255.0 blue:204/255.0 alpha:1.0]
#define kColorFea2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorEsalq1 [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]
#define kColorEsalq2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorSanFran1 [UIColor colorWithRed:212/255.0 green:37/255.0 blue:30/255.0 alpha:1.0]
#define kColorSanFran2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorMed1 [UIColor colorWithRed:0/255.0 green:100/255.0 blue:0/255.0 alpha:1.0]
#define kColorMed2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorOdonto1 [UIColor colorWithRed:2/255.0 green:40/255.0 blue:78/255.0 alpha:1.0]
#define kColorOdonto2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorTodos1 [UIColor colorWithRed:0/255.0 green:0/255.0 blue:51/255.0 alpha:1.0]
#define kColorTodos2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]

#define kStdDefMainColor @"IUMainColor"
#define kStdDefTextColor @"IUTextColor"
#define kStdDefSelectedTeam @"IUSelectedTeam"
#define kStdDefSelectedTeamName @"IUSelectedTeamName"


#pragma mark - Singleton
static UserConfig *_shared = nil;

+(UserConfig*)shared{
    @synchronized(self)
    {
        if (_shared == nil) {
            _shared = [[self alloc]init];
            [_shared loadStandardDefaults];
        }
        return _shared;
    }
}

-(void)selectTeam:(IUTeam)team{
    self.mainColor = [UIColor colorWithRed:0.0 green:0.0 blue:51.0/255.0 alpha:1.0];
    self.textColor = [UIColor whiteColor];
    switch (team) {
        case IUTeamPoli:
            self.mainColor = kColorPoli1;
            self.textColor = kColorPoli2;
            self.selectedTeamName = @"POLI";
            break;
        case IUTeamFea:
            self.mainColor = kColorFea1;
            self.textColor = kColorFea2;
            self.selectedTeamName = @"FEA";
            break;
        case IUTeamFarma:
            self.mainColor = kColorFarma1;
            self.textColor = kColorFarma2;
            self.selectedTeamName = @"FARMA";
            break;
        case IUTeamOdonto:
            self.mainColor = kColorOdonto1;
            self.textColor = kColorOdonto2;
            self.selectedTeamName = @"FEA";
            break;
        case IUTeamPinheiros:
            self.mainColor = kColorMed1;
            self.textColor = kColorMed2;
            self.selectedTeamName = @"PINHEIROS";
            break;
        case IUTeamRibeirao:
            self.mainColor = kColorRibeirao1;
            self.textColor = kColorRibeirao2;
            self.selectedTeamName = @"RIBEIRAO";
            break;
        case IUTeamSanFran:
            self.mainColor = kColorSanFran1;
            self.textColor = kColorSanFran2;
            self.selectedTeamName = @"SANFRAN";
            break;
        case IUTeamESALQ:
            self.mainColor = kColorEsalq1;
            self.textColor = kColorEsalq2;
            self.selectedTeamName = @"ESALQ";
            break;
            
        default:
            self.selectedTeamName = @"NENHUM";
            break;
    }
    
    [[UINavigationBar appearance] setBackgroundColor:self.mainColor];
    [[UINavigationBar appearance] setTintColor:self.textColor];
    
    self.selectedTeam = team;
    [self saveStandardDefaults];
}

-(void)saveStandardDefaults{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setInteger:self.selectedTeam forKey:kStdDefSelectedTeam];
    [standardUserDefaults setObject:self.selectedTeamName forKey:kStdDefSelectedTeamName];
    [standardUserDefaults synchronize];
}

-(void)loadStandardDefaults{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    self.selectedTeam = [standardUserDefaults integerForKey:kStdDefSelectedTeam];
    self.selectedTeamName = [standardUserDefaults objectForKey:kStdDefSelectedTeamName];
    [self selectTeam:self.selectedTeam];
}

@end
