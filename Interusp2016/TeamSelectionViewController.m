//
//  TeamSelectionViewController.m
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "TeamSelectionViewController.h"
#import "MainTabController.h"
#import "UserConfig.h"

@interface TeamSelectionViewController ()

    @property (nonatomic, weak) IBOutlet UIView * coverView;

@end

@implementation TeamSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    if ([UserConfig shared].selectedTeam != 0) {
        MainTabController * mainTabs = [MainTabController initMainTabs];
        [self presentViewController:mainTabs animated:false completion:^{}];
    } else {
        [self.coverView removeFromSuperview];
    }
}

#pragma mark - Private Methods

-(void)nextView{
    MainTabController * mainTabs = [MainTabController initMainTabs];
    [self presentViewController:mainTabs animated:true completion:^{}];
    
}

#pragma mark - IBActions

-(IBAction)selectPOLI:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamPoli];
    [self nextView];
}

-(IBAction)selectFEA:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamFea];
    [self nextView];
}

-(IBAction)selectESALQ:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamESALQ];
    [self nextView];
}
-(IBAction)selectPINHEIROS:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamPinheiros];
    [self nextView];
}
-(IBAction)selectODONTO:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamOdonto];
    [self nextView];
}
-(IBAction)selectRIBEIRAO:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamRibeirao];
    [self nextView];
}
-(IBAction)selectSANFRAN:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamSanFran];
    [self nextView];
}
-(IBAction)selectFARMA:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamFarma];
    [self nextView];
}

-(IBAction)selectNone:(id)sender{
    [[UserConfig shared] selectTeam:IUTeamNone];
    [self nextView];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
