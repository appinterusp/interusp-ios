//
//  JogosViewController.m
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "JogosViewController.h"
#import "JogoManager.h"
#import "Jogo.h"
#import "JogoCell.h"
#import "UserConfig.h"

@interface JogosViewController () <JogoManagerDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) JogoManager * jogoManager;
@property (nonatomic, strong) NSArray * jogoList;
@property (nonatomic, weak) IBOutlet UITableView * table;
@property (nonatomic, weak) IBOutlet UIButton * btnDia, *btnModalidade, *btnAtletica, *btnLocal;

@property (nonatomic, strong) UIActionSheet * aSheetData, *aSheetModalidade, *aSheetTeam, *aSheetLocal;

@end

@implementation JogosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.jogoList = [[NSArray alloc] init];
    self.jogoManager = [[JogoManager alloc] init];
    self.jogoManager.delegate = self;
    [self.jogoManager getJogos];
    
    self.title = @"Jogos";

    [self setupTheme];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DynamicLayout

-(void)setupTheme{
    self.btnAtletica.backgroundColor = [UserConfig shared].mainColor;
    [self.btnAtletica setTitleColor:[UserConfig shared].textColor forState:UIControlStateNormal];
    self.btnDia.backgroundColor = [UserConfig shared].mainColor;
    [self.btnDia setTitleColor:[UserConfig shared].textColor forState:UIControlStateNormal];
    self.btnLocal.backgroundColor = [UserConfig shared].mainColor;
    [self.btnLocal setTitleColor:[UserConfig shared].textColor forState:UIControlStateNormal];
    self.btnModalidade.backgroundColor = [UserConfig shared].mainColor;
    [self.btnModalidade setTitleColor:[UserConfig shared].textColor forState:UIControlStateNormal];
}

#pragma mark - IBActions

-(IBAction)filterData:(id)sender{
    if (!self.aSheetData) {
        self.aSheetData = [[UIActionSheet alloc] initWithTitle:@"Selecione a data" delegate:self cancelButtonTitle:@"Todos" destructiveButtonTitle:nil otherButtonTitles:@"26/05",@"27/05",@"28/05",@"29/05", nil];
    }
    [self.aSheetData showFromTabBar:self.tabBarController.tabBar];
}

-(IBAction)filterModalidade:(id)sender{
    if (!self.aSheetModalidade) {
        self.aSheetModalidade = [[UIActionSheet alloc] initWithTitle:@"Selecione a modalidade" delegate:self cancelButtonTitle:@"Todos" destructiveButtonTitle:nil otherButtonTitles:@"Atletismo Masculino", @"Atletismo Feminino", @"Beisebol", @"Basquete Masculino",@"Basquete Feminino",@"Futebol de Campo",@"Futsal Masculino", @"Futsal Feminino", @"Handebol Masculino", @"Handebol Feminino", @"Judô", @"Karatê", @"Natação Masculina",@"Natação Feminina",@"Polo Aquático",@"Rugby Masculino",@"Rugby Feminino",@"Softbol",@"Tênis de Campo Masculino",@"Tênis de Campo Feminino",@"Tênis de Mesa Masculino",@"Tênis de Mesa Feminino",@"Voleibol Masculino",@"Voleibol Feminino",@"Xadrez",nil];
    }
    [self.aSheetModalidade showFromTabBar:self.tabBarController.tabBar];
}

-(IBAction)filterAtletica:(id)sender{
    if (!self.aSheetTeam) {
        self.aSheetTeam = [[UIActionSheet alloc] initWithTitle:@"Selecione a atlética" delegate:self cancelButtonTitle:@"Todos" destructiveButtonTitle:nil otherButtonTitles:@"POLI",@"FEA",@"ODONTO",@"RIBEIRÃO", @"FARMA", @"PINHEIROS", @"ESALQ", @"SANFRAN", nil];
    }
    [self.aSheetTeam showFromTabBar:self.tabBarController.tabBar];
}

-(IBAction)filterLocal:(id)sender{
    if (!self.aSheetLocal) {
        self.aSheetLocal = [[UIActionSheet alloc] initWithTitle:@"Selecione o local" delegate:self cancelButtonTitle:@"Todos" destructiveButtonTitle:nil otherButtonTitles:@"G1",@"G2",@"G3",@"Campo 1", @"Campo 2", @"Campo 3", @"Campo 4", @"Piscina", @"Pista de Atletismo", nil];
    }
    [self.aSheetLocal showFromTabBar:self.tabBarController.tabBar];
}

-(void)openLocal
{
    if (!self.aSheetLocal) {
        self.aSheetLocal = [[UIActionSheet alloc] initWithTitle:@"Selecione o local" delegate:self cancelButtonTitle:@"Todos" destructiveButtonTitle:nil otherButtonTitles:@"G1",@"G2",@"G3",@"Campo 1", @"Campo 2", @"Campo 3", @"Campo 4", @"Piscina", @"Pista de Atletismo", nil];
    }
    [self.aSheetLocal showFromTabBar:self.tabBarController.tabBar];
}

#pragma mark - ActionSheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSString * filter;
    NSString * buttonTitle;
    UIButton * btn;
    if (actionSheet == self.aSheetData) {
        filter = @"dia";
        buttonTitle = @"Dia";
        btn = self.btnDia;
    } else if (actionSheet == self.aSheetModalidade) {
        filter = @"modalidade";
        buttonTitle = @"Modalidade";
        btn = self.btnModalidade;
    } else if (actionSheet == self.aSheetTeam) {
        filter = @"atletica";
        buttonTitle = @"Atletica";
        btn = self.btnAtletica;
    } else if (actionSheet == self.aSheetLocal) {
        filter = @"local";
        buttonTitle = @"Local";
        btn = self.btnLocal;
    } else {
        return;
    }
    
    if (buttonIndex == [actionSheet cancelButtonIndex]) {
        [self.jogoManager configFilter:filter withValue:nil];
    } else {
        buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
        [self.jogoManager configFilter:filter withValue: buttonTitle];
        if (actionSheet == self.aSheetModalidade) {
            buttonTitle = [self shortNameForSport:buttonTitle];
        }
        
    }
    [btn setTitle:buttonTitle forState:UIControlStateNormal];
}

-(NSString*)shortNameForSport:(NSString*)sportName{
    NSDictionary * shortnameDict = @{
    @"Atletismo Masculino":@"AM",
    @"Atletismo Feminino":@"AF",
    @"Beisebol":@"BSB",
    @"Basquete Masculino":@"BM",
    @"Basquete Feminino":@"BF",
    @"Futebol de Campo":@"FC",
    @"Futsal Masculino":@"FSM",
    @"Futsal Feminino":@"FSF",
    @"Handebol Masculino":@"HM",
    @"Handebol Feminino":@"HF",
    @"Judô":@"JD",
    @"Karatê":@"KRT",
    @"Natação Masculina":@"NM",
    @"Natação Feminina":@"NF",
    @"Polo Aquático":@"PA",
    @"Rugby Masculino":@"RM",
    @"Rugby Feminino":@"RF",
    @"Softbol":@"SB",
    @"Tênis de Campo Masculino":@"TCM",
    @"Tênis de Campo Feminino":@"TCF",
    @"Tênis de Mesa Masculino":@"TMM",
    @"Tênis de Mesa Feminino":@"TMF",
    @"Voleibol Masculino":@"VM",
    @"Voleibol Feminino":@"VF",
    @"Xadrez":@"XDZ"};
    
    NSString * shortName = [shortnameDict objectForKey:sportName];
    return shortName;
}

#pragma mark - JogoManagerDelegate

-(void)jogoManagerDidUpdateGames:(NSArray *)gameList{
    self.jogoList = gameList;
    [self.table reloadData];
}

-(void)jogoManagerError:(NSString *)errorString{
    //TODO
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.jogoList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Jogo * jogoModel = [self.jogoList objectAtIndex:indexPath.row];
    
    JogoCell * cell = [self.table dequeueReusableCellWithIdentifier:@"JOGOCELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"JogoCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithModel:jogoModel];
    return cell;
}

-(void)refresh
{
    NSLog(@"REFRESH");
    [self.jogoManager getJogos];
}

@end
