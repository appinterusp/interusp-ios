//
//  OnibusDataManager.m
//  Interusp2016
//
//  Created by Luciano Naganawa on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "OnibusDataManager.h"
#import "WebService.h"
#import "Onibus.h"
@implementation OnibusDataManager


-(void)getOnibus{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    int collegeType = [defaults integerForKey:@"IUSelectedTeam"];
    NSMutableArray *temp = [NSMutableArray array];
    [WebService getBusByCollege:collegeType withBlock:^(ServiceResult *result) {
        NSMutableArray *array = [NSMutableArray array];
        array = [result.resultDictionary objectForKey:@"onibus"];
        for (NSDictionary *dic in array){
            Onibus *newbus = [[Onibus alloc] init];
            newbus.placa = dic[@"placa"];
            newbus.informacoes = dic[@"informacoes"];
            [temp addObject:newbus];
        }
        [[self delegate] getLocationsFinished:[NSArray arrayWithArray:temp]];

        
    }];

}
@end
