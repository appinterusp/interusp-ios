//
//  ChaveamentoDetailViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ChaveamentoDetailViewController.h"
#import "UserConfig.h"
#import "ChaveView.h"
#import "WebService.h"

@interface ChaveamentoDetailViewController ()

@property (nonatomic, strong) NSString * modalidadeString;
@property int modalidadeCode;
@property (nonatomic, strong) NSArray * gameList;

@property (nonatomic, weak) IBOutlet UILabel * lblTitle;
@property (nonatomic, weak) IBOutlet UIView * chaveContainer;

@property (nonatomic, strong) ChaveView * chaveView;

@end

@implementation ChaveamentoDetailViewController

#pragma mark - Constructor

+(ChaveamentoDetailViewController*)initWithModalidade:(NSString*)modalidadeString andCode:(int)modalidadeCode{
    ChaveamentoDetailViewController * instance = [[ChaveamentoDetailViewController alloc] initWithNibName:@"ChaveamentoDetailViewController" bundle:nil];
    if (instance) {
        instance.modalidadeString = modalidadeString;
        instance.modalidadeCode = modalidadeCode;
        //instance.modalidadeCode = 1;
    }
    return instance;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"";
    
    self.lblTitle.text = self.modalidadeString;
    self.lblTitle.backgroundColor = [UserConfig shared].mainColor;
    self.lblTitle.textColor = [UserConfig shared].textColor;
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ChaveView" owner:nil options:nil];
    if ([arrayOfViews count] < 1) return;
    self.chaveView = (ChaveView*)[arrayOfViews objectAtIndex:0];
    [self.chaveContainer addSubview:self.chaveView];
    self.chaveView.center = CGPointMake(CGRectGetMidX(self.chaveContainer.bounds), CGRectGetMidY(self.chaveContainer.bounds));

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [WebService getGamesForModalidade:self.modalidadeCode withBlock:^(ServiceResult *result) {
        if (result.succeeded) {
            self.gameList = (NSArray*)[result.resultDictionary objectForKey:@"jogos"];
            [self.chaveView parseToView:self.gameList];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
