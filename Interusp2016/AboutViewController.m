//
//  AboutViewController.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/4/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "AboutViewController.h"
#import "AboutTableViewCell.h"

@interface AboutViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *nomes;
@property (nonatomic, strong) IBOutlet UITableView *table;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setText];
    
    self.table.delegate = self;
    self.table.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.nomes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.nomes objectAtIndex:indexPath.row];
    
    AboutTableViewCell * cell = (AboutTableViewCell*)[self.table dequeueReusableCellWithIdentifier:@"AboutTableViewCell"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"AboutTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTitle:viewString];
    return cell;
}

-(IBAction)goToSite:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.appsimples.com.br/"]];
}

-(void)setText
{
    self.nomes = [NSArray arrayWithObjects:
                             @"Victor Kazuyuki Shinya",
                             @"Joao Sisanoski",
                             @"Gabriel Oliveira",
                             @"Camila Yumi Mizutani",
                             @"Luciano Naganawa",
                             @"Caio Massaki Souza Naganawa",
                             @"Ricardo Mutti",
                             @"Rafael Camargos Santos",
                             @"Agnaldo Bernegozzi",
                             @"Luis Felipe Viveiros Fusco",
                             @"Yargo Vó Tessaro",
                             @"Fernando Nassif",
                             @"Diego Fabri Abrahão",
                             @"Felipe Gonçalves Marques",
                             @"Wendler Boaventura Eis",
                             @"André Wada Gromann",
                             @"Bruno Arakaki",
                             @"Mauricio Casanova Nogueira",
                             @"Bruno Galvão",
                             @"Daniel Fabri Abrahão",
                             @"Rafael Hiroki Yoshimura Kamada",
                             @"Alexandre Ciuffatelli",
                             @"Guilherme Adissy",
                             @"Michel Chieregato",
                             @"Gabriel Moreira Minossi",
                             @"Camila Miyuki",
                             @"Rafael Brandão",
                             nil];
}

@end
