//
//  MapViewController.m
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import "LocationManager.h"
#import "LocationHelper.h"
#import "LocationManagerData.h"
#import "MKCustomAnnotation.h"
#import "MKCustomPointAnnotation.h"
#import "LoadingView.h"
#import "DetalhesLocalViewController.h"

@interface MapViewController () <MKMapViewDelegate, LocationManagerDataDelegate>{
    LocationManager *locationManager;
    NSArray *arrayOfLocations;
    LocationManagerData *locData;
    LoadingView * loadingView;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *gmaps;
@property (weak, nonatomic) MKCustomPointAnnotation *selectedLocal;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    locationManager = [[LocationManager alloc] init];
    
    [locationManager startUpdateLocation];
    _mapView.delegate = self;
    _mapView.showsUserLocation = true;
    self.mapView.showsUserLocation = YES;

    locData = [[LocationManagerData alloc] init];
    locData.delegate = self;
    loadingView = [LoadingView addLoadingViewToView:self.view];
    [locData getLocations];
    
    self.gmaps.hidden = true;

    

}
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
   
}

-(void)addPointMap{
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    for (Local *loc in arrayOfLocations){
        CLLocationCoordinate2D c2D = CLLocationCoordinate2DMake(loc.latitude,loc.longitude);

        MKCustomPointAnnotation *point = [[MKCustomPointAnnotation alloc] init];
        point.coordinate = c2D;
        point.title = loc.name;
        point.local = loc;
        point.subtitle = [LocationHelper getStringForPlaceType:loc.placeType];
        [[self mapView] addAnnotation:point];
        [annotations addObject:point];
    }
    [self.mapView showAnnotations:annotations animated:YES];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    static NSString *pinIdentifier = @"SFAnnotationIdentifier";
    
    if ([annotation isKindOfClass:[MKUserLocation class]]){
        return nil;
    }
    MKCustomPointAnnotation *point = annotation;
    
    MKCustomAnnotation *pinView =
    (MKCustomAnnotation *)[mapView dequeueReusableAnnotationViewWithIdentifier:pinIdentifier];
    if (!pinView){
        pinView = [[MKCustomAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:pinIdentifier];
    }
    pinView.annotation = annotation;
    pinView.loc = point.local;
    pinView.canShowCallout = true;
    UIImage *flagImage = [LocationHelper getImageForPlaceType:[point local].placeType];
    pinView.image = flagImage;
    pinView.rightCalloutAccessoryView = [self yesButtonWithLocal:point.local];
    return pinView;
}
-(void)didTapButton:(UIButton *)sender
{
    if (arrayOfLocations.count>sender.tag) {
        Local * selectedLocal = [arrayOfLocations objectAtIndex:sender.tag];
        DetalhesLocalViewController *detalhes = [[DetalhesLocalViewController alloc] init];
        detalhes.local = selectedLocal;
        [self.navigationController pushViewController:detalhes animated:true];
    }
}
- (UIButton *)yesButtonWithLocal: (Local *)local {
    #warning para trocar a imagem do balao do mapa aqui.
    UIImage *image = [UIImage imageNamed:@"added"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, image.size.width, image.size.height); // don't use auto layout
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = [arrayOfLocations indexOfObject:local];
    return button;
}
#pragma mark LocationDataDelegate
-(void)getLocationsFinished:(NSArray *)array{
    arrayOfLocations = array;
    [self addPointMap];
    [loadingView removeFromSuperview];
    loadingView = nil;

}
-(void)errorThrowed:(NSError *)error{
    [loadingView removeFromSuperview];
    loadingView = nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    self.selectedLocal = [self.mapView selectedAnnotations].firstObject;
    self.gmaps.hidden = false;
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    self.selectedLocal = nil;
    self.gmaps.hidden = true;
}


-(IBAction)openGmaps:(id)sender
{
    if (self.selectedLocal) {
        //Google Maps
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"comgooglemaps://?q=%f,%f", self.selectedLocal.coordinate.latitude, self.selectedLocal.coordinate.longitude]];
        
        if (![[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]) {
            NSLog(@"Google Maps app is not installed");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/en/app/google-maps/id585027354?mt=8"]];
            //TRATAR ERRO DO GOOGLE
        } else {
            [[UIApplication sharedApplication] openURL:url];
        }
    }

}

@end
