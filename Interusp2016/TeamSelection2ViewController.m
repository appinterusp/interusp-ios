//
//  TeamSelection2ViewController.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "TeamSelection2ViewController.h"
#import "UserConfig.h"
#import "MainTabController.h"
#import "WebService.h"

@interface TeamSelection2ViewController ()

@property (nonatomic, strong) IBOutlet UIImageView *image;
@property (nonatomic, strong) IBOutlet UIButton *btnContinuar;
@property (nonatomic, strong) IBOutlet UIButton *btnRight;
@property (nonatomic, strong) IBOutlet UIButton *btnLeft;
@property (nonatomic, strong) IBOutlet UILabel *lblTodos;
@property (nonatomic) int index;
@property (nonatomic, strong) NSArray *atleticas;
@property (nonatomic, weak) IBOutlet UIView * coverView;
@property (nonatomic, strong) IBOutlet UIView * container;

@end

@implementation TeamSelection2ViewController

#define kColorPoli2 [UIColor colorWithRed:243/255.0 green:251/255.0 blue:5/255.0 alpha:1.0]
#define kColorPoli1 [UIColor colorWithRed:23/255.0 green:27/255.0 blue:64/255.0 alpha:1.0]
#define kColorRibeirao1 [UIColor colorWithRed:9/255.0 green:59/255.0 blue:36/255.0 alpha:1.0]
#define kColorRibeirao2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorFarma1 [UIColor colorWithRed:130/255.0 green:21/255.0 blue:24/255.0 alpha:1.0]
#define kColorFarma2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorFea1 [UIColor colorWithRed:0/255.0 green:0/255.0 blue:204/255.0 alpha:1.0]
#define kColorFea2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorEsalq1 [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]
#define kColorEsalq2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorSanFran1 [UIColor colorWithRed:212/255.0 green:37/255.0 blue:30/255.0 alpha:1.0]
#define kColorSanFran2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorMed1 [UIColor colorWithRed:0/255.0 green:100/255.0 blue:0/255.0 alpha:1.0]
#define kColorMed2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorOdonto1 [UIColor colorWithRed:2/255.0 green:40/255.0 blue:78/255.0 alpha:1.0]
#define kColorOdonto2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define kColorTodos1 [UIColor colorWithRed:0/255.0 green:0/255.0 blue:51/255.0 alpha:1.0]
#define kColorTodos2 [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]

#define kStdDefMainColor @"IUMainColor"
#define kStdDefTextColor @"IUTextColor"
#define kStdDefSelectedTeam @"IUSelectedTeam"

#define kIUTeamSent @"IUTeamSent"

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.container.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    self.index = 0;
    
    self.atleticas = [NSArray arrayWithObjects:
                             @"poli.png",
                             @"esalq.png",
                             @"farma.png",
                             @"fea.png",
                             @"medrib.png",
                             @"odonto.png",
                             @"sanfran.png",
                             @"pinheiros.png",
                             nil];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(shiftRight:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.image addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(shiftLeft:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.image addGestureRecognizer:swipeLeft];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated{
    if ([UserConfig shared].selectedTeam != 0) {
        MainTabController * mainTabs = [MainTabController initMainTabs];
        [self presentViewController:mainTabs animated:false completion:^{}];
    } else {
        [self.coverView removeFromSuperview];
    }
}

-(IBAction)shiftLeft:(id)sender
{
    if (self.index == 0) {
        self.index = 8;
    } else {
        self.index--;
    }
    
    if (self.index == 0) {
        self.lblTodos.text = @"Continuar sem Atética";
        self.image.image = [UIImage imageNamed:@""];
    } else {
        self.lblTodos.text = @"";
        self.image.image = [UIImage imageNamed:self.atleticas[self.index-1]];
    }
    
    [self setButtonColor];
}

-(IBAction)shiftRight:(id)sender
{
    if (self.index == 8) {
        self.index = 0;
    } else {
        self.index++;
    }
    
    if (self.index == 0) {
        self.lblTodos.text = @"Continuar sem Atética";
        self.image.image = [UIImage imageNamed:@""];
    } else {
        self.lblTodos.text = @"";
        self.image.image = [UIImage imageNamed:self.atleticas[self.index-1]];
    }
    
    [self setButtonColor];
}

-(void)setButtonColor
{
    switch (self.index) {
        case 0:
            [self.btnContinuar setTitleColor:kColorTodos2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorTodos1];
            [self.btnLeft setTitleColor:kColorTodos1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorTodos1 forState:UIControlStateNormal];
            break;
        case 1:
            [self.btnContinuar setTitleColor:kColorPoli2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorPoli1];
            [self.btnLeft setTitleColor:kColorPoli1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorPoli1 forState:UIControlStateNormal];
            break;
        case 2:
            [self.btnContinuar setTitleColor:kColorEsalq2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorEsalq1];
            [self.btnLeft setTitleColor:kColorEsalq1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorEsalq1 forState:UIControlStateNormal];
            break;
        case 3:
            [self.btnContinuar setTitleColor:kColorFarma2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorFarma1];
            [self.btnLeft setTitleColor:kColorFarma1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorFarma1 forState:UIControlStateNormal];
            break;
        case 4:
            [self.btnContinuar setTitleColor:kColorFea2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorFea1];
            [self.btnLeft setTitleColor:kColorFea1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorFea1 forState:UIControlStateNormal];
            break;
        case 5:
            [self.btnContinuar setTitleColor:kColorRibeirao2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorRibeirao1];
            [self.btnLeft setTitleColor:kColorRibeirao1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorRibeirao1 forState:UIControlStateNormal];
            break;
        case 6:
            [self.btnContinuar setTitleColor:kColorOdonto2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorOdonto1];
            [self.btnLeft setTitleColor:kColorOdonto1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorOdonto1 forState:UIControlStateNormal];
            break;
        case 7:
            [self.btnContinuar setTitleColor:kColorSanFran2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorSanFran1];
            [self.btnLeft setTitleColor:kColorSanFran1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorSanFran1 forState:UIControlStateNormal];
            break;
        case 8:
            [self.btnContinuar setTitleColor:kColorMed2 forState:UIControlStateNormal];
            [self.btnContinuar setBackgroundColor:kColorMed1];
            [self.btnLeft setTitleColor:kColorMed1 forState:UIControlStateNormal];
            [self.btnRight setTitleColor:kColorMed1 forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

-(IBAction)selecionarAtletica:(id)sender
{
    switch (self.index) {
        case 0:
            [[UserConfig shared] selectTeam:IUTeamNone];
            break;
        case 1:
            [[UserConfig shared] selectTeam:IUTeamPoli];
            break;
        case 2:
            [[UserConfig shared] selectTeam:IUTeamESALQ];
            break;
        case 3:
            [[UserConfig shared] selectTeam:IUTeamFarma];
            break;
        case 4:
            [[UserConfig shared] selectTeam:IUTeamFea];
            break;
        case 5:
            [[UserConfig shared] selectTeam:IUTeamRibeirao];
            break;
        case 6:
            [[UserConfig shared] selectTeam:IUTeamOdonto];
            break;
        case 7:
            [[UserConfig shared] selectTeam:IUTeamSanFran];
            break;
        case 8:
            [[UserConfig shared] selectTeam:IUTeamPinheiros];
            break;
        default:
            break;
    }
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (self.index != 0 && ![standardUserDefaults boolForKey:kIUTeamSent]) {
        [WebService addTracking:self.index WithBlock:^(ServiceResult *result) {
            if (result.succeeded) {
                [standardUserDefaults setBool:TRUE forKey:kIUTeamSent];
            }
        }];
    }
    MainTabController * mainTabs = [MainTabController initMainTabs];
    [self presentViewController:mainTabs animated:true completion:^{}];
}


@end
