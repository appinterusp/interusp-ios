//
//  ScoreModalidadeCell.h
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreModalidadeCell : UITableViewCell

-(void)configWithTeam:(UIImage*)team score:(NSString*)score position:(NSString*)position myTeam:(BOOL)myTeam;

@end
