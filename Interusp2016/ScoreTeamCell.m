//
//  ScoreTeamCell.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ScoreTeamCell.h"

@interface ScoreTeamCell ()

@property (nonatomic, weak) IBOutlet UIImageView * imgLogo;
@property (nonatomic, weak) IBOutlet UILabel * lblScore, *lblPosition;

@end

@implementation ScoreTeamCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self clean];
}

-(void)clean{
    self.imgLogo.image = nil;
    self.lblScore.text = nil;
    self.lblPosition.text = nil;
}

-(void)configWithSport:(UIImage*)sport score:(NSString*)score position:(NSString*)position{
    self.imgLogo.image = sport;
    self.lblScore.text = score;
    self.lblPosition.text = position;
}

@end
