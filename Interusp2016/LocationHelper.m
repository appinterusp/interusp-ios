//
//  LocationHelper.m
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "LocationHelper.h"

@implementation LocationHelper
+(NSString *)getStringForPlaceType:(IUPlaceType)placeType{
    switch (placeType) {
        case IUPlaceGym:
            return @"Ginásio";
        case IUPlaceTendas:
            return @"Tenda";
        case IUPlaceBalada:
            return @"Balada";
        case IUPlaceOnibus:
            return @"Ônibus";
        case IUPlaceAlojamento:
            return @"Alojamento";
        case IUPlaceHospital:
            return @"Hospital";
        case IUPlaceDelegacia:
            return @"Delegacia";
        case IUPlaceRestaurantes:
            return @"Restaurante";
       
        default:
            return @"";

    }
}
+(UIImage *)getImageForPlaceType:(IUPlaceType)placeType{
    NSString *nameImage;
    #warning para trocar a imagem para cada tipo no mapa aqui.
    switch (placeType) {
        case IUPlaceGym:
            nameImage =  @"ginasios2";
            break;
        case IUPlaceTendas:
            nameImage =  @"tenda2";
            break;
        case IUPlaceBalada:
            nameImage =  @"baladas2";
            break;
        case IUPlaceOnibus:
            nameImage =  @"onibus2";
            break;
        case IUPlaceAlojamento:
            nameImage =  @"alojamento2";
            break;
        case IUPlaceHospital:
            nameImage =  @"hospital2";
            break;
        case IUPlaceDelegacia:
            nameImage =  @"delegacia2";
            break;
        case IUPlaceRestaurantes:
            nameImage =  @"comidas2";
            break;
            
        default:
            nameImage =  @"comidas";
            break;
            
    }
    return [UIImage imageNamed:nameImage];
}

@end
