//
//  HistoricoDetailViewController.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/4/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "HistoricoDetailViewController.h"
#import "UserConfig.h"

@interface HistoricoDetailViewController ()

@property (nonatomic, strong) IBOutlet UILabel * lblModalide;
@property (nonatomic, strong) IBOutlet UILabel *lblAno1, *lblAno2, *lblAno3, *lblAno4, *lblAno5, *lblAno6, *lblAno7, *lblAno8, *lblAno9, *lblAno10;

@end

@implementation HistoricoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.lblModalide setTextColor:[UserConfig shared].mainColor];
    self.lblModalide.text = self.modalidade;
    
//    if (self.resultados.count < 5) {
//        [self setSmallResults];
//    } else {
//        [self setLargeResults];
//    }
    [self setLabel:self.lblAno1 index:0];
    [self setLabel:self.lblAno2 index:1];
    [self setLabel:self.lblAno3 index:2];
    [self setLabel:self.lblAno4 index:3];
    [self setLabel:self.lblAno5 index:4];
    [self setLabel:self.lblAno6 index:5];
    [self setLabel:self.lblAno7 index:6];
    [self setLabel:self.lblAno8 index:7];
    [self setLabel:self.lblAno9 index:8];
    [self setLabel:self.lblAno10 index:9];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setLabel:(UILabel*)label index:(int)index
{
    if (self.resultados.count > index) {
        NSString *value = [self.resultados objectAtIndex:index];
        label.text = value;
        if ([value rangeOfString:[UserConfig shared].selectedTeamName].location != NSNotFound) {
            label.backgroundColor = [UserConfig shared].mainColor;
            label.textColor = [UserConfig shared].textColor;
        }
    }
}

//-(void)setSmallResults
//{
//    self.lblAno1.text = [self.resultados objectAtIndex:0];
//    self.lblAno2.text = [self.resultados objectAtIndex:1];
//    self.lblAno3.text = [self.resultados objectAtIndex:2];
//    
//    if (self.resultados.count > 3) {
//        self.lblAno4.hidden = false;
//        self.lblAno4.text = [self.resultados objectAtIndex:3];
//    }
//}
//
//-(void)setLargeResults
//{
//    self.lblAno1.text = [self.resultados objectAtIndex:0];
//    self.lblAno2.text = [self.resultados objectAtIndex:1];
//    self.lblAno3.text = [self.resultados objectAtIndex:2];
//    self.lblAno4.hidden = false;
//    self.lblAno4.text = [self.resultados objectAtIndex:3];
//    self.lblAno5.hidden = false;
//    self.lblAno5.text = [self.resultados objectAtIndex:4];
//    self.lblAno6.hidden = false;
//    self.lblAno6.text = [self.resultados objectAtIndex:5];
//    self.lblAno7.hidden = false;
//    self.lblAno7.text = [self.resultados objectAtIndex:6];
//    self.lblAno8.hidden = false;
//    self.lblAno8.text = [self.resultados objectAtIndex:7];
//    self.lblAno9.hidden = false;
//    self.lblAno9.text = [self.resultados objectAtIndex:8];
//    self.lblAno10.hidden = false;
//    self.lblAno10.text = [self.resultados objectAtIndex:9];
//}

@end
