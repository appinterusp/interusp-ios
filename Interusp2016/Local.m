//
//  Local.m
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "Local.h"

@implementation Local
+(Local*)initWithDictionary:(NSDictionary*)dictionary{
    Local * newLocal = [[Local alloc] init];
    newLocal.name = [dictionary objectForKey:@"nome"];
    newLocal.imageURL = [dictionary objectForKey:@"foto"];
    newLocal.latitude = [[[dictionary objectForKey:@"coordenadas"] objectAtIndex:1] floatValue];
    newLocal.longitude = [[[dictionary objectForKey:@"coordenadas"] objectAtIndex:0] floatValue];
    newLocal.endereco = [dictionary objectForKey:@"descricao"];
    newLocal.placeType = [[dictionary objectForKey:@"tipo"] intValue];
    
    return newLocal;
}

+(NSArray*)localListWithJSON:(NSArray*)jsonArray{
    NSMutableArray * list = [[NSMutableArray alloc] init];
    for (NSDictionary * d in jsonArray) {
        Local * l = [Local initWithDictionary:d];
        [list addObject:l];
    }
    return [NSArray arrayWithArray:list];
}

@end
