//
//  ScoreGeralViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ScoreGeralViewController.h"
#import "ScoreGeralCell.h"
#import "ScoreGeralHeader.h"
#import "ScoreTeamViewController.h"
#import "WebService.h"
#import "SelectModalidadeViewController.h"

@interface ScoreGeralViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView * table;
@property (nonatomic, strong) NSArray * scoreList;

@end

@implementation ScoreGeralViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*self.scoreList = @[@{@"team":@"poli", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"},
                       @{@"team":@"fea", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"},
                       @{@"team":@"farma", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"},
                       @{@"team":@"odonto", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"},
                       @{@"team":@"medrib", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"},
                       @{@"team":@"pinheiros", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"},
                       @{@"team":@"sanfran", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"},
                       @{@"team":@"esalq", @"score":@"1", @"minScore":@"99", @"maxScore":@"120"}];*/
    
    [WebService getPontuaçãoWithBlock:^(ServiceResult *result) {
        if (result.succeeded) {
            self.scoreList = [result.resultDictionary objectForKey:@"faculdades"];
            [self.table reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)selectModalidade:(id)sender{
    SelectModalidadeViewController * vc = [[SelectModalidadeViewController alloc] initWithNibName:@"SelectModalidadeViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.scoreList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * model = [self.scoreList objectAtIndex:indexPath.row];
    
    ScoreGeralCell * cell = [self.table dequeueReusableCellWithIdentifier:@"SCORECELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"ScoreGeralCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTeam:[model objectForKey:@"nome"] score:[[model objectForKey:@"pontuacao_atual"] stringValue] minScore:[[model objectForKey:@"pontuacao_min"] stringValue] maxScore:[[model objectForKey:@"pontuacao_max"] stringValue]];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ScoreGeralHeader2" owner:nil options:nil];
    if ([arrayOfViews count] < 1) return nil;
    ScoreGeralHeader *header = (ScoreGeralHeader*)[arrayOfViews objectAtIndex:0];
    
    return header;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * model = [self.scoreList objectAtIndex:indexPath.row];
    int faculdadeID = [[model objectForKey:@"id"] intValue];
    
    ScoreTeamViewController *vc = [[ScoreTeamViewController alloc] initWithNibName:@"ScoreTeamViewController" bundle:nil];
    vc.faculdadeId = faculdadeID;
    [self.navigationController pushViewController:vc animated:true];
    
}

@end
