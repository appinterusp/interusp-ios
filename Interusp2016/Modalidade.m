//
//  Modalidade.m
//  Interusp2016
//
//  Created by AppSimples on 5/2/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "Modalidade.h"

@implementation Modalidade

+(Modalidade*)initWithNome:(NSString *)nome andID:(int)modalidadeID{
    Modalidade * instance = [[Modalidade alloc] init];
    instance.nome = nome;
    instance.modalidadeID = modalidadeID;
    if (modalidadeID==1||modalidadeID==2||modalidadeID==12||modalidadeID==13||modalidadeID==14) {
        instance.hasChave = 0;
    } else {
        instance.hasChave = 1;
    }
    return instance;
}

+(NSArray *)allModalidades{
    NSMutableArray * mArray = [[NSMutableArray alloc] init];
    [mArray addObject:[Modalidade initWithNome:@"Atletismo Feminino" andID:1]];
    [mArray addObject:[Modalidade initWithNome:@"Atletismo Masculino" andID:2]];
    [mArray addObject:[Modalidade initWithNome:@"Basquete Feminino" andID:3]];
    [mArray addObject:[Modalidade initWithNome:@"Basquete Masculino" andID:4]];
    [mArray addObject:[Modalidade initWithNome:@"Beisebol" andID:5]];
    [mArray addObject:[Modalidade initWithNome:@"Futebol de Campo" andID:6]];
    [mArray addObject:[Modalidade initWithNome:@"Futsal Feminino" andID:7]];
    [mArray addObject:[Modalidade initWithNome:@"Futsal Masculino" andID:8]];
    [mArray addObject:[Modalidade initWithNome:@"Handebol Feminino" andID:9]];
    [mArray addObject:[Modalidade initWithNome:@"Handebol Masculino" andID:10]];
    [mArray addObject:[Modalidade initWithNome:@"Judô" andID:11]];
    [mArray addObject:[Modalidade initWithNome:@"Karatê" andID:12]];
    [mArray addObject:[Modalidade initWithNome:@"Natação Feminina" andID:13]];
    [mArray addObject:[Modalidade initWithNome:@"Natação Masculina" andID:14]];
    [mArray addObject:[Modalidade initWithNome:@"Polo Aquático" andID:15]];
    [mArray addObject:[Modalidade initWithNome:@"Rugby" andID:16]];
    [mArray addObject:[Modalidade initWithNome:@"Softbol" andID:17]];
    [mArray addObject:[Modalidade initWithNome:@"Tênis de Campo Feminino" andID:18]];
    [mArray addObject:[Modalidade initWithNome:@"Tênis de Campo Masculino" andID:19]];
    [mArray addObject:[Modalidade initWithNome:@"Tênis de Mesa Feminino" andID:20]];
    [mArray addObject:[Modalidade initWithNome:@"Tênis de Mesa Masculino" andID:21]];
    [mArray addObject:[Modalidade initWithNome:@"Voleibol Feminino" andID:22]];
    [mArray addObject:[Modalidade initWithNome:@"Voleibol Masculino" andID:23]];
    [mArray addObject:[Modalidade initWithNome:@"Xadrez" andID:24]];
    [mArray addObject:[Modalidade initWithNome:@"Rugby Feminino" andID:25]];
    
    return [NSArray arrayWithArray:mArray];
}

+(NSArray *)modalidadesChave{
    NSMutableArray * mArray = [[NSMutableArray alloc] init];
    NSArray * aArray = [Modalidade allModalidades];
    
    for (Modalidade * m in aArray) {
        if (m.hasChave) {
            [mArray addObject:m];
        }
    }
    
    return [NSArray arrayWithArray:mArray];
}

@end
