//
//  LocationManager.h
//  Swap
//
//  Created by Luciano Naganawa on 4/11/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Local.h"
#import "UserConfig.h"
@protocol LocationManagerDelegate
@optional
-(void)authorizationStatusDenied;
-(void)authorizationStatusNotDetermined;
-(void)updateLocation:(CLLocation *)userLocation;
-(void)getUsersNearFinished:(NSArray *)array;
-(void)authorizationStatusAccepted;


@end
@interface LocationManager : NSObject <CLLocationManagerDelegate>
@property float intervalTime;
@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic,weak)  id <LocationManagerDelegate> delegate;

-(void)startUpdateLocation;
@end
