//
//  InfoViewController.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/4/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@property (nonatomic, strong) IBOutlet UITextView *textView;

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setText];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setText
{
    self.textView.text = @"\tEm 2016 será realizada a 32º edição do InterUSP. Tão tradicional quanto as atléticas que delas participam, o InterUSP é uma das competições universitárias mais competitivas do país. ESALQ, Farmácia, FEA, Medicina, Pinheiros, Medicina Ribeirão, Odontologia, Poli e São Fransico disputam, durante quatr dias, em uma cidade do interior de São Paulo, 25 modalidades.\n\n"
    
    "\tNenhuma competição tem tanto esporte em tão alto nivel quanto a InterUSP. Todas as modalidades são obrigatórias e em cada uma dela uma rivalidade particular se destaca tornando todas elas atrativas. A parte social também se destaca: o público cresce a cada ano tanto tanto nas arquibancadas quanto nas festas. As torcidas apaixonadas deixam a rivalidade de lado nas duas festas organizadas pelas Atléticas.\n\n"
    
    "Como os Jogos Funcionam?\n\n"
    
    "A contagem de pontos da Inter/usp é feita por modalidade, de acordo com a classificação atlética. \n\n1º - 13 Pontos \n2º - 10 Pontos \n3º - 8 Pontos \n4º - 6 Pontos \n5º - 4 Pontos \n6º - 3 Pontos \n7º - 2 Pontos \n8º - 1 Ponto\n\n"
    
    "Os perdedores nas semifinais e quartas de final para o campeão serão, respectivamente 3º e 5° colocados.\n\n"
    
    "Os perdedores nas quartas de final para o 3° e 4° colocados serão, respectivamente 7º e 8° colocados.\n\n"
    
    "A atlética vencedora é aquela que somar mais pontos considerando todas as modadlidades.\n\n"
    
    "Em caso de empate, a campeã será a com maior número de primeiros lugares, em seguida, segundos lugares e assim por diante.\n\n"
    
    "Caso o empate persista, será a atlética com maior número de vitórias em confrontos diretos entre as duas atléticas empatadas.\n\n"
    
    "Caso o empate ainda persista, ambas serão declaradas campeãs.\n\n";
    
    [self.textView setContentOffset:CGPointMake(0, 0)];
}


@end
