//
//  WebConstants.h
//  Swap
//
//  Created by Luciano Naganawa on 4/6/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#ifndef WebConstants_h
#define WebConstants_h


#define URL_WS @"http://ec2-52-201-135-248.compute-1.amazonaws.com:3000/v1/"
#define GET_PLACES_ENDPOINT @"locais/"
#define GET_JOGOS @"jogo/"
#define ONIBUS @"onibus?faculdade="
#define PLACES_BY_TYPE @"places"
#define PLACE_TYPE @"place_type"
#define GET_FACULDADES @"faculdade/"
#define GET_TORCIDAS @"tracking/"

#endif
