//
//  GritoLetraViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/3/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "GritoLetraViewController.h"

@interface GritoLetraViewController ()

@property (nonatomic, strong) NSString * letraDoGrito;

@property (nonatomic, weak) IBOutlet UILabel * lblTitle;
@property (nonatomic, weak) IBOutlet UITextView * txtViewLetra;

@end

@implementation GritoLetraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if ([self.grito isEqualToString:@"Grito"]) {
        self.letraDoGrito = @"Letra do Grito";
    }
    if ([self.grito isEqualToString:@"Guerreiro"]) {
        self.letraDoGrito = @"Eu sou um guerreiro que sozinho bate em mil\nEu sou da POLI a mais fudida do Brasil\n\nSe é pra matar\nSe é pra morrer\nOs ratos da POLI tão botando pra fuder\n\nPorrada! Porrada! Porrada!\n\nEu sou, eu sou da POLI, eu sou\nVou dar porrada, eu vou\nE ninguém vai me segurar (nem a PM)";
    }
    if ([self.grito isEqualToString:@"Poli Maravilha"]) {
        self.letraDoGrito = @"E na InterUSP,\nSempre que você jogava\nToda torcida enlouquecida\nTe empurrava (2x)\n\nPoli Maravilha,\nNós torcemos por você\nPoli Maravilha,\nTraz o troféu pra gente ver (2x)";
    }
    if ([self.grito isEqualToString:@"Horto Mágico"]) {
        self.letraDoGrito = @"ÔÔÔÔÔ\nPOLI ÔÔÔÔ\nCanto por ti o meu amor\n\nTemida és tu\nMinha Paixão\nSou tua torcida, tua voz e coração..\n\nPOLI chegou\nPreste atenção\nEu vim aqui para ser o campeão\n\nTemida és tu\nMinha Paixão\nSou tua torcida, tua voz e coração!";
    }
    if ([self.grito isEqualToString:@"Bata Amarela"]) {
        self.letraDoGrito = @"POLI\nEstaremos contigo\nTu és minha paixão!\nNão importa o que digam\nSempre levarei comigo\n\nMinha bata amarela\nE a caneca na mão!\nA InterUSP me espera\nVai começar a guerra!\n\nXalaialaiaaaaa (3x)\nPOLI do meu coração!\nXalaialaiaaaaa (3x)\nNa Med só tem cuzão!";
    }
    if ([self.grito isEqualToString:@"Incessante"]) {
        self.letraDoGrito = @"Viemos de São Paulo\nE muitas vezes tivemos que parar\nParar, parar, parar pra dá porrada\nDá porrada na GV\nDá porrada na GV\nGV, vai se fuder!\nÉ a TOFUUUU, pau no seu c*\n\nLelele, leleleo\nTOFU tá vindo ai pra botar muito terror (x2)\n\nLevanta a arquibancada\nNão para de agitar\nNão tem medo de morrer\nDá porrada sem parar\n\nE no Economíadas\nO esquema é sem noção\nTOFU é incessante\nDá porrada de montão!";
    }
    if ([self.grito isEqualToString:@"Funk do Cachorro"]) {
        self.letraDoGrito = @"TOFU\n(palmas)\nTOFU\n(palmas)\nTOFU\n(palmas)\nTOFUUUUUU\n\nÉ o Funk do Cachorro!\nAh, aqui só tem cachorro loco\nAh, quero beber, quero cheirar\nCuidado, PUC, INSPER e GV,\nPorque, a TOFU vai te pegar\n\nÔ pega ela TOFU\nPegs Pegs\nÔ pega ela TOFU\nPegs Pegs\nÔ pega ela TOFU\nPegs Pegs\nÔ pega ela TOFU\nPegs Pegs";
    }
    if ([self.grito isEqualToString:@"Tralalalaô"]) {
        self.letraDoGrito = @"Ô, tralalalaô\nÔ, tralalalaô\nÔ, tralalalala, hey\nTralalalala, hey, tralalalalaô\nAs flores já não crescem mais\nAté o alecrim murchou\nA sapo se mandou\nO lambari morreu\nPorque o ribeirão secou";
    }
    if ([self.grito isEqualToString:@"Fea USP Maravilhosa"]) {
        self.letraDoGrito = @"FEA-USP Maravilhosa, cheia de encantos mil\nFEA-USP Maravilhosa, melhor escola do Brasil (x2)\n\nEssa é a escola que todos desejam\nMas nem todos podem entrar\nVocê que tentou\nE não conseguiu\nQue vá pra p*** que o pariu";
    }
    if ([self.grito isEqualToString:@"Hino do Agricolão"]) {
        self.letraDoGrito = @"Nós somos da agronomia\nFiés paus d’água\nQue porcaria\nAndamos sempre de bonde\nCom o juízo não sei aonde\nMas quando as coisas se apertam\nE nós pensamos no amanhã\nA turma toda padece\nQuando floresce\nO flamboyant!\nPã rã pã pã\n\nA pinga queremos com limão!\nMulheres, com muita animação!\nPorém se a Pátria amada\nPrecisar do Agricolão\nAi ai ai que papelão!\nPã rã pã pã\n\nComo é sublime bebe uma Brahma\nE depois fumar uma palha\nnuma bela duma grama\nAMOR FEBRIL, Ô Ô Ô Ô\nPelo barril, ôôôô\nNão há quem possa\nCom a turma nossa!\n\nA Pinga queremos com fervor\nMulheres! com muito mais amor\nPorém se a Patria amada\nPrecisar de agricolada\nPuta merda que cagada!\n \nArei, gradeei, aduba que adubando dá! ESALQ! ESALQ! Rá rá rá! (2x)";
    }
    if ([self.grito isEqualToString:@"Odonto Minha Paixão"]) {
        self.letraDoGrito = @"FOUSP maravilhosa\nCheia de encantos mil\nFOUSP maravilhosa\nMelhor escola do Brasil!\n\nEssa é a escola\nQue todos desejam\nMas poucos podem entrar\nVocê que tentou \nE não conseguiu\nVai pra p*** que pariu!";
    }
    if ([self.grito isEqualToString:@"FOUSP Maravilhosa"]) {
        self.letraDoGrito = @"Odonto\nEstaremos contigo \nSomos uma nação \nNão importa onde esteja\nSempre levarei comigo\n\nO meu manto sagrado \nE a cachaça na mão \nA torcida me espera \nPra começar a festa \n\nXa laia laia, xa laia laia, xa laia laia\nOdonto minha paixao (2x)";
    }
    if ([self.grito isEqualToString:@"Ami"]) {
        self.letraDoGrito = @"Miiiiiiii....\nVamos nessa\nVamos beber essa cerveja\nFarmácia mi, farmácia mi, farmacia miiiii (2x)\n\nVamos beber essa cerveja\nÉ testa é mi, é nariz mi, é boca é mi, é queixo é mi, é peito é mi, é sexo é mi, É GULA!";
    }
    
    
    self.txtViewLetra.text = self.letraDoGrito;
    self.lblTitle.text = self.grito;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
