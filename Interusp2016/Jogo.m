//
//  Jogo.m
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "Jogo.h"

@implementation Jogo


+(Jogo*)initWithDictionary:(NSDictionary*)dictionary{
    Jogo * newJogo = [[Jogo alloc] init];
    newJogo.horario = [dictionary objectForKey:@"horario"];
    newJogo.title = [dictionary objectForKey:@"title"];
    newJogo.subtitle = [dictionary objectForKey:@"subtitle"];
    newJogo.local = [dictionary objectForKey:@"local"];
    newJogo.participantes = [dictionary objectForKey:@"participantes"];
    newJogo.dataString = [dictionary objectForKey:@"data"];
    newJogo.data = [Jogo dataFromString:newJogo.dataString];
    newJogo.modalidade = [dictionary objectForKey:@"modalidade"];
    
    return newJogo;
}

+(Jogo*)initWithServiceResultDict:(NSDictionary*)dictionary{
    Jogo * newJogo = [[Jogo alloc] init];
    newJogo.dataString = [dictionary objectForKey:@"data"];
    newJogo.title = [dictionary objectForKey:@"descricao"];
    newJogo.subtitle = [dictionary objectForKey:@"nome"];
    newJogo.local = [dictionary objectForKey:@"local"];
    newJogo.modalidadeCode = [[dictionary objectForKey:@"modalidade_id"] intValue];
    newJogo.modalidade = [Jogo sportNameForCode:newJogo.modalidadeCode];
    
    NSMutableArray * mArray = [[NSMutableArray alloc] init];
    if ([dictionary objectForKey:@"faculdade_1"]) {
        [mArray addObject:[Jogo teamNameForCode:[[dictionary objectForKey:@"faculdade_1"] intValue]]];
    }
    if ([dictionary objectForKey:@"faculdade_2"]) {
        [mArray addObject:[Jogo teamNameForCode:[[dictionary objectForKey:@"faculdade_2"] intValue]]];
    }
    newJogo.participantes = [NSArray arrayWithArray:mArray];
    
    newJogo.horario = [newJogo.dataString substringWithRange:NSMakeRange(11, 5)];
    newJogo.dia = [[newJogo.dataString substringWithRange:NSMakeRange(8, 2)] stringByAppendingString:@"/05"];
    
    newJogo.isProva = [[dictionary objectForKey:@"is_prova"] intValue];
    
    return newJogo;
}

+(NSString*)sportNameForCode:(int)code{
    NSString * name = @"";
    switch (code) {
        case 1:
            name = @"Atletismo Feminino";
            break;
            
        case 2:
            name = @"Atletismo Masculino";
            break;
            
        case 3:
            name = @"Basquete Feminino";
            break;
            
        case 4:
            name = @"Basquete Masculino";
            break;
            
        case 5:
            name = @"Beisebol";
            break;
            
        case 6:
            name = @"Futebol de Campo";
            break;
            
        case 7:
            name = @"Futsal Feminino";
            break;
            
        case 8:
            name = @"Futsal Masculino";
            break;
            
        case 9:
            name = @"Handebol Feminino";
            break;
            
        case 10:
            name = @"Handebol Masculino";
            break;
            
        case 11:
            name = @"Judô";
            break;
            
        case 12:
            name = @"Karatê";
            break;
            
        case 13:
            name = @"Natação Feminina";
            break;
            
        case 14:
            name = @"Natação Masculina";
            break;
            
        case 15:
            name = @"Polo Aquático";
            break;
            
        case 16:
            name = @"Rugby Masculino";
            break;
            
        case 17:
            name = @"Softbol";
            break;
            
        case 18:
            name = @"Tênis de Campo Feminino";
            break;
            
        case 19:
            name = @"Tênis de Campo Masculino";
            break;
            
        case 20:
            name = @"Tênis de Mesa Feminino";
            break;
            
        case 21:
            name = @"Tênis de Mesa Masculino";
            break;
            
        case 22:
            name = @"Voleibol Feminino";
            break;
            
        case 23:
            name = @"Voleibol Masculino";
            break;
            
        case 24:
            name = @"Xadrez";
            break;
            
        case 25:
            name = @"Rugby Feminino";
            break;
            
        default:
            break;
    }
    return name;
}

+(NSString*)teamNameForCode:(int)code{
    NSString * name = @"";
    switch (code) {
        case 1:
            name = @"POLI";
            break;
            
        case 2:
            name = @"FEA";
            break;
            
        case 3:
            name = @"FARMA";
            break;
            
        case 4:
            name = @"ESALQ";
            break;
            
        case 5:
            name = @"RIBEIRÃO";
            break;
            
        case 6:
            name = @"SANFRAN";
            break;
            
        case 7:
            name = @"ODONTO";
            break;
            
        case 8:
            name = @"PINHEIROS";
            break;

        default:
            break;
    }
    return name;
}

+(NSDate*)dataFromString:(NSString*)string{
    if ([string isEqualToString:@"27/05"]) {
        return [Jogo dateWithYear:2016 month:5 day:27];
    }
    if ([string isEqualToString:@"28/05"]) {
        return [Jogo dateWithYear:2016 month:5 day:28];
    }
    if ([string isEqualToString:@"29/05"]) {
        return [Jogo dateWithYear:2016 month:5 day:29];
    }
    return [Jogo dateWithYear:2016 month:5 day:30];
}

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    return [calendar dateFromComponents:components];
}



@end
