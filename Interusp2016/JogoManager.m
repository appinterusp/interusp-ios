//
//  JogoManager.m
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "JogoManager.h"
#import "Jogo.h"
#import "WebService.h"

@interface JogoManager()

@property (nonatomic, strong) NSArray * gameList;
@property (nonatomic, strong) NSMutableArray * filteredGameList;
@property (nonatomic, strong) NSMutableDictionary * filters;

@end

@implementation JogoManager

#pragma mark - PublicMethods

-(void)getJogos{
    //THIS IS PLACEHOLDER
    //self.gameList = [JogoManager placeholderGames];
    //[self filterArray];
    //[self.delegate jogoManagerDidUpdateGames:self.filteredGameList];
    
    [WebService getGamesForModalidade:0 withBlock:^(ServiceResult *result) {
        
        //pegar array do result
        //remover jogos "vencedores"
        
        self.gameList = [JogoManager parseGameList:(NSArray*)[result.resultDictionary objectForKey:@"jogos"]];
        [self filterArray];
        [self.delegate jogoManagerDidUpdateGames:self.filteredGameList];
    }];
}

-(void)configFilter:(NSString*)filter withValue:(NSString*)value{
    if (!self.filters) {
        self.filters = [[NSMutableDictionary alloc] init];
    }
    if (!value) {
        [self.filters removeObjectForKey:filter];
    } else {
        [self.filters setObject:value forKey:filter];
    }
    
    [self filterArray];
    [self.delegate jogoManagerDidUpdateGames:self.filteredGameList];
}

#pragma mark - PrivateMethods

-(void)filterArray{
    self.filteredGameList = [[NSMutableArray alloc] init];
    for (Jogo * aJogo in self.gameList) {
        BOOL shouldAdd = true;
        if ([self.filters objectForKey:@"dia"] && shouldAdd) {
            shouldAdd = [aJogo.dia isEqualToString:[self.filters objectForKey:@"dia"]];
        }
        if ([self.filters objectForKey:@"local"] && shouldAdd) {
            BOOL b = [aJogo.local rangeOfString:[self.filters objectForKey:@"local"] options:NSCaseInsensitiveSearch].location != NSNotFound;
            shouldAdd = b;
        }
        if ([self.filters objectForKey:@"modalidade"] && shouldAdd) {
            shouldAdd = [aJogo.modalidade isEqualToString:[self.filters objectForKey:@"modalidade"]];
        }
        if ([self.filters objectForKey:@"atletica"] && shouldAdd) {
            shouldAdd = [aJogo.participantes indexOfObject:[self.filters objectForKey:@"atletica"]]!= NSNotFound;
        }
        if (shouldAdd) {
            [self.filteredGameList addObject:aJogo];
        }
    }
}

#pragma mark - Placeholder

+(NSArray*)placeholderGames{
    NSMutableArray * mArray = [[NSMutableArray alloc] init];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"8:00",@"title":@"POL X FAR",@"subtitle":@"quartas-de-final", @"local":@"G1", @"participantes":@[@"POLI",@"FARMA"], @"data":@"26/05", @"modalidade":@"FSM"}]];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"10:00",@"title":@"FEA X ODONTO",@"subtitle":@"quartas-de-final", @"local":@"G1", @"participantes":@[@"FEA",@"ODONTO"], @"data":@"26/05", @"modalidade":@"FSF"}]];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"11:00",@"title":@"POL X FAR",@"subtitle":@"quartas-de-final", @"local":@"G1", @"participantes":@[@"POLI",@"FARMA"], @"data":@"27/05", @"modalidade":@"HF"}]];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"12:00",@"title":@"POL X FEA",@"subtitle":@"quartas-de-final", @"local":@"G1", @"participantes":@[@"POLI",@"FEA"], @"data":@"27/05", @"modalidade":@"FSF"}]];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"8:00",@"title":@"POL X FAR",@"subtitle":@"quartas-de-final", @"local":@"G2", @"participantes":@[@"POLI",@"FARMA"], @"data":@"28/05", @"modalidade":@"HF"}]];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"10:00",@"title":@"FEA X ODONTO",@"subtitle":@"quartas-de-final", @"local":@"G3", @"participantes":@[@"FEA",@"ODONTO"], @"data":@"28/05", @"modalidade":@"FSF"}]];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"11:00",@"title":@"POL X FAR",@"subtitle":@"quartas-de-final", @"local":@"G3", @"participantes":@[@"POLI",@"FARMA"], @"data":@"29/05", @"modalidade":@"FC"}]];
    [mArray addObject:[Jogo initWithDictionary:@{@"horario":@"12:00",@"title":@"FEA X ODONTO",@"subtitle":@"quartas-de-final", @"local":@"G2", @"participantes":@[@"FEA",@"ODONTO"], @"data":@"29/05", @"modalidade":@"FC"}]];
    return [NSArray arrayWithArray:mArray];
}

+(NSArray*)parseGameList:(NSArray*)gameList{
    NSMutableArray * mArray = [[NSMutableArray alloc] init];
    for (NSDictionary * dict in gameList) {
        if (![[dict objectForKey:@"is_vencedor"] intValue]) {
            Jogo * j = [Jogo initWithServiceResultDict:dict];
            [mArray addObject:j];
        }
    }
    return [NSArray arrayWithArray:mArray];
}

@end
