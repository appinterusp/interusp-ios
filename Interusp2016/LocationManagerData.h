//
//  LocationManagerData.h
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Local.h"
@protocol LocationManagerDataDelegate
@optional
-(void)getLocationsFinished:(NSArray *)array;
-(void)errorThrowed:(NSError *)error;

@end
@interface LocationManagerData : NSObject
@property id <LocationManagerDataDelegate> delegate;

-(void)getLocations;
-(NSArray *)getLocationsSpecifiedType:(IUPlaceType)type;

@end
