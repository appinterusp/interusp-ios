//
//  Local.h
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationHelper.h"
@interface Local : NSObject
@property NSString *name;
@property IUPlaceType placeType;
@property float latitude;
@property float longitude;
@property UIImage *image;
@property (nonatomic, strong) NSString * imageURL;
@property NSString *endereco;
@property int identificador;

+(Local*)initWithDictionary:(NSDictionary*)dictionary;
+(NSArray*)localListWithJSON:(NSArray*)jsonArray;

@end
