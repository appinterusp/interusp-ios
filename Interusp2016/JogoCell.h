//
//  JogoCell.h
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jogo.h"

@interface JogoCell : UITableViewCell

-(void)configWithModel:(Jogo *)model;

@end
