//
//  AboutTableViewCell.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/4/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "AboutTableViewCell.h"

@interface AboutTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel * lblTitle;

@end

@implementation AboutTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self clean];
}

-(void)clean{
    self.lblTitle.text = nil;
}

-(void)configWithTitle:(NSString *)title{
    self.lblTitle.text = title;
}

@end
