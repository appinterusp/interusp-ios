//
//  PontuacaoGeralTableViewCell.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ScoreGeralCell.h"

@interface ScoreGeralCell ()

@property (nonatomic, weak) IBOutlet UIImageView * imgLogo;
@property (nonatomic, weak) IBOutlet UILabel * lblScore, *lblMinScore, *lblMaxScore;

@end

@implementation ScoreGeralCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self clean];
}

-(void)clean{
    self.imgLogo.image = nil;
    self.lblScore.text = nil;
    self.lblMinScore.text = nil;
    self.lblMaxScore.text = nil;
}

-(void)configWithTeam:(NSString*)team score:(NSString*)score minScore:(NSString*)minScore maxScore:(NSString*)maxScore{
    self.imgLogo.image = [self imageForName:team];
    self.lblScore.text = score;
    self.lblMinScore.text = minScore;
    self.lblMaxScore.text = maxScore;
}

-(UIImage *)imageForName:(NSString*)name{
    if ([name isEqualToString:@"Poli"]) {
        return [UIImage imageNamed:@"poli"];
    }
    if ([name isEqualToString:@"Pinheiros"]) {
        return [UIImage imageNamed:@"pinheiros"];
    }
    if ([name isEqualToString:@"FEA"]) {
        return [UIImage imageNamed:@"fea"];
    }
    if ([name isEqualToString:@"Odonto"]) {
        return [UIImage imageNamed:@"odonto"];
    }
    if ([name isEqualToString:@"Farma"]) {
        return [UIImage imageNamed:@"farma"];
    }
    if ([name isEqualToString:@"SanFran"]) {
        return [UIImage imageNamed:@"sanfran"];
    }
    if ([name isEqualToString:@"Ribeirão"]) {
        return [UIImage imageNamed:@"medrib"];
    }
    if ([name isEqualToString:@"ESALQ"]) {
        return [UIImage imageNamed:@"esalq"];
    }
    return nil;
}

@end
