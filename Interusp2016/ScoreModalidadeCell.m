//
//  ScoreModalidadeCell.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ScoreModalidadeCell.h"
#import "UserConfig.h"

@interface ScoreModalidadeCell ()

@property (nonatomic, weak) IBOutlet UIImageView * imgLogo;
@property (nonatomic, weak) IBOutlet UILabel * lblScore, *lblPosition;

@end



@implementation ScoreModalidadeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self clean];
}

-(void)clean{
    self.imgLogo.image = nil;
    self.lblScore.text = nil;
    self.lblPosition.text = nil;
}

-(void)configWithTeam:(UIImage*)team score:(NSString*)score position:(NSString*)position myTeam:(BOOL)myTeam{
    self.imgLogo.image = team;
    self.lblScore.text = score;
    self.lblPosition.text = position;
    if (myTeam) {
        self.contentView.backgroundColor = [[UserConfig shared].mainColor colorWithAlphaComponent:0.2f];;
        
    }
}

@end
