//
//  OnibusTableViewCell.m
//  Interusp2016
//
//  Created by Luciano Naganawa on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "OnibusTableViewCell.h"

@implementation OnibusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}
-(void)setPlacaText: (NSString *)string{
    [[self placeLbl] setText:string];
}
-(void)setDescricaoText:(NSString *)string{
    [[self descricaoLbl] setText:string];
}
-(void)confWithOnibus:(Onibus *)onibus{
    [self setPlacaText:[onibus placa]];
    [self setDescricaoText:[onibus informacoes]];
}

@end
