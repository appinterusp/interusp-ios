//
//  TorcidaCell.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "TorcidaCell.h"


@interface TorcidaCell ()

@property (nonatomic, weak) IBOutlet UILabel * lblName;
@property (nonatomic, weak) IBOutlet UILabel * lblTotal;
@property (nonatomic, weak) IBOutlet UIImageView * imgBar;

#define kColorPoli [UIColor colorWithRed:243/255.0 green:251/255.0 blue:5/255.0 alpha:1.0]
#define kColorRibeirao [UIColor colorWithRed:9/255.0 green:59/255.0 blue:36/255.0 alpha:1.0]
#define kColorFarma [UIColor colorWithRed:130/255.0 green:21/255.0 blue:24/255.0 alpha:1.0]
#define kColorFea [UIColor colorWithRed:0/255.0 green:0/255.0 blue:204/255.0 alpha:1.0]
#define kColorEsalq [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]
#define kColorSanFran [UIColor colorWithRed:212/255.0 green:37/255.0 blue:30/255.0 alpha:1.0]
#define kColorMed [UIColor colorWithRed:0/255.0 green:100/255.0 blue:0/255.0 alpha:1.0]
#define kColorOdonto [UIColor colorWithRed:2/255.0 green:40/255.0 blue:78/255.0 alpha:1.0]

@end

@implementation TorcidaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configWithModel:(Torcida *)model andTotal:(int)total{
    
    switch (model.faculID) {
        case 1:
            self.lblName.text = @"POLI";
            self.imgBar.backgroundColor = kColorPoli;
            break;
        case 2:
            self.lblName.text = @"FEA";
            self.imgBar.backgroundColor = kColorFea;
            break;
        case 3:
            self.lblName.text = @"FARMA";
            self.imgBar.backgroundColor = kColorFarma;
            break;
        case 4:
            self.lblName.text = @"ESALQ";
            self.imgBar.backgroundColor = kColorEsalq;
            break;
        case 5:
            self.lblName.text = @"RIBEIRÃO";
            self.imgBar.backgroundColor = kColorRibeirao;
            break;
        case 6:
            self.lblName.text = @"SANFRAN";
            self.imgBar.backgroundColor = kColorSanFran;
            break;
        case 7:
            self.lblName.text = @"ODONTO";
            self.imgBar.backgroundColor = kColorOdonto;
            break;
        case 8:
            self.lblName.text = @"PINHEIROS";
            self.imgBar.backgroundColor = kColorMed;
            break;
            
        default:
            break;
    }
    
    NSString *count = [NSString stringWithFormat:@"%d", model.fanCount];
    self.lblTotal.text = count;
    
    CGFloat tam = (model.fanCount * self.imgBar.frame.size.width)/total;
    if (model.fanCount == 0) {
        //tam = 0.5;
        self.imgBar.backgroundColor = UIColor.whiteColor;
    } else {
        self.imgBar.frame = CGRectMake(self.imgBar.frame.origin.x, self.imgBar.frame.origin.y, tam, self.imgBar.frame.size.height);
    }
    
}


@end
