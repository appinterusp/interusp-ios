
//
//  LocationManager.m
//  Swap
//
//  Created by Luciano Naganawa on 4/11/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "LocationManager.h"
#import "AppDelegate.h"

@implementation LocationManager

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
}
-(instancetype)init{
    self = [super init];
    if (self ){
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return self;
}
-(void)runMethod

{
     [_locationManager startUpdatingLocation];
}
-(void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            [self.delegate authorizationStatusNotDetermined];
        } break;
        case kCLAuthorizationStatusDenied: {
            [self.delegate authorizationStatusDenied];
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            
            
        } break;
        default:
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    [self.delegate updateLocation:location];
    //NSLog(@"%@", location);


}
-(void)startUpdateLocation{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [_locationManager requestWhenInUseAuthorization];
        [self runMethod];
    }

}

@end
