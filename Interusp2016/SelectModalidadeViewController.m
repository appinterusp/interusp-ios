//
//  SelectModalidadeViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "SelectModalidadeViewController.h"
#import "ScoreSportViewController.h"

@interface SelectModalidadeViewController ()

@end

@implementation SelectModalidadeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)openDetailModalidade:(id)sender{
    ScoreSportViewController * vc = [[ScoreSportViewController alloc] initWithNibName:@"ScoreSportViewController" bundle:nil];
    vc.modalidadeId = [(UIButton*)sender tag];
    [self.navigationController pushViewController:vc animated:true];
}

@end
