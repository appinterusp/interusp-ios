//
//  TorcidaCell.h
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Torcida.h"

@interface TorcidaCell : UITableViewCell

-(void)configWithModel:(Torcida *)model andTotal:(int)total;

@end
