//
//  Modalidade.h
//  Interusp2016
//
//  Created by AppSimples on 5/2/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Modalidade : NSObject

@property (nonatomic, strong) NSString * nome;
@property int modalidadeID;
@property BOOL hasChave;

+(Modalidade*)initWithNome:(NSString *)nome andID:(int)modalidadeID;
+(NSArray *)modalidadesChave;
+(NSArray *)allModalidades;

@end
