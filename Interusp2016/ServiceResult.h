//
//  ServiceResult.h
//  Swap
//
//  Created by AppSimples on 3/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceResult : NSObject
typedef enum {
    NoUser,
    NotConnectServerDown,
    Error,
} ErrorType;
+(ServiceResult*)initWithStatus:(NSNumber*)statusCode message:(NSString*)message andResult:(NSDictionary*)resultDictionary;
+(ServiceResult*)initWithJSon:(NSDictionary*)resultDictionary;

@property (nonatomic) BOOL succeeded;
@property (nonatomic, strong) NSError * error;
@property (nonatomic, strong) NSDictionary * resultDictionary;
@property (nonatomic, strong) NSString * message;

@end
