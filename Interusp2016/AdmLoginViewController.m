//
//  AdmLoginViewController.m
//  Interusp2016
//
//  Created by App Simples on 01/05/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "AdmLoginViewController.h"

@interface AdmLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField * txtLogin;
@property (weak, nonatomic) IBOutlet UITextField * txtSenha;
@property (weak, nonatomic) IBOutlet UIButton * btnLogin;

@end

@implementation AdmLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.btnLogin.layer.cornerRadius = self.btnLogin.frame.size.height / 2;
    self.btnLogin.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)login:(id)sender {
    if ([self.txtLogin.text length] > 0 && [self.txtSenha.text length] > 0) {
        
    } else if ([self.txtLogin.text length] < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Campo Vazio" message:@"Campo login vazio. Digite um login valido." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
    } else if ([self.txtSenha.text length] < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Campo Vazio" message:@"Campo senha vazia. Digite uma senha valida." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro no login" message:@"Login/Senha incorreta" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
