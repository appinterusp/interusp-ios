//
//  ScoreSportViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ScoreSportViewController.h"
#import "WebService.h"
#import "ScoreModalidadeCell.h"
#import "ScoreSportHeader.h"
#import "UserConfig.h"

@interface ScoreSportViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView * table;
@property (nonatomic, weak) IBOutlet UILabel * modalidade;
@property (nonatomic, strong) NSArray * lista;
@property (nonatomic, strong) NSArray * atleticas;

@end

@implementation ScoreSportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modalidade.textColor = [UserConfig shared].mainColor;
    
    self.atleticas = [NSArray arrayWithObjects:
                      @"poli.png",
                      @"fea.png",
                      @"farma.png",
                      @"esalq.png",
                      @"medrib.png",
                      @"sanfran.png",
                      @"odonto.png",
                      @"pinheiros.png",
                      nil];
    
    [WebService getPontuaçãoModalidade:self.modalidadeId WithBlock:^(ServiceResult *result) {
        if (result.succeeded) {
            NSArray * a = [result.resultDictionary objectForKey:@"modalidades"];
            if (a && a.count>0) {
                NSDictionary * d = [a objectAtIndex:0];
                self.modalidade.text = [d objectForKey:@"nome"];
                self.lista = [d objectForKey:@"pontuacao_total"];
                [self.table reloadData];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lista.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * model = [self.lista objectAtIndex:indexPath.row];
    NSString *posicao = [[model objectForKey:@"posicao"] stringValue];
    int faculdade = [[model objectForKey:@"faculdade"] intValue]-1;
    UIImage *atletica = [UIImage imageNamed:[self.atleticas objectAtIndex:faculdade]];
    NSString *pontos = [[model objectForKey:@"pontuacao"] stringValue];
    BOOL myTeam = ([[model objectForKey:@"faculdade"] intValue] == [UserConfig shared].selectedTeam);
    
    ScoreModalidadeCell * cell = [self.table dequeueReusableCellWithIdentifier:@"SCORECELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"ScoreModalidadeCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTeam:atletica score:pontos position:posicao myTeam:myTeam];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ScoreSportHeader" owner:nil options:nil];
    if ([arrayOfViews count] < 1) return nil;
    ScoreSportHeader *header = (ScoreSportHeader*)[arrayOfViews objectAtIndex:0];
    
    return header;
}


@end
