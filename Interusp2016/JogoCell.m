//
//  JogoCell.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "JogoCell.h"
#import "UserConfig.h"

@interface JogoCell ()

@property (nonatomic, weak) IBOutlet UILabel * lblTitle;
@property (nonatomic, weak) IBOutlet UILabel * lblModalidade, *lblData;

@end


@implementation JogoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self clean];
}

-(void)clean{
    self.lblTitle.text = nil;
    self.lblModalidade.text = nil;
    self.lblData.text = nil;
}

-(void)configWithModel:(Jogo *)model{
    self.lblTitle.text = model.title;
    self.lblTitle.textColor = [UserConfig shared].mainColor;
    self.lblModalidade.text = [NSString stringWithFormat:@"%@ - %@",model.modalidade,model.subtitle];
    self.lblData.text = [NSString stringWithFormat:@"%@ - %@ - %@", model.dia, model.horario, model.local];
    
    if (model.isProva) {
        self.lblTitle.text = @"Todas as atléticas";
    } else {
        if (model.participantes) {
            if (model.participantes.count==0) {
                self.lblTitle.text = @"Indefinido";
            } else {
                self.lblTitle.text = [[model.participantes objectAtIndex:0] stringByAppendingString:@" - "];
                if (model.participantes.count>1) {
                    self.lblTitle.text = [self.lblTitle.text stringByAppendingString:[model.participantes objectAtIndex:1]];
                }
            }
        }
    }

}

@end
