//
//  OnibusViewController.m
//  Interusp2016
//
//  Created by Luciano Naganawa on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "OnibusViewController.h"
#import "Onibus.h"
#import "OnibusTableViewCell.h"
@interface OnibusViewController (){
    OnibusDataManager *_onibusDataManager;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSString *reuse;
@end

@implementation OnibusViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _reuse  = @"OnibusCell";
    [self.tableView registerNib:[UINib nibWithNibName:@"OnibusTableViewCell" bundle:nil] forCellReuseIdentifier:_reuse];
    _tableView.estimatedRowHeight = 160.0;
    
    _onibusDataManager = [[OnibusDataManager alloc] init];
    _onibusDataManager.delegate = self;
    [_onibusDataManager getOnibus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self arrayOfInfos] count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OnibusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_reuse];
    if (!cell){
        cell = [[OnibusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:_reuse];
    }
    Onibus *onibus = [[self arrayOfInfos] objectAtIndex:indexPath.row];
   
    [cell confWithOnibus:onibus];
    return cell;
    
}
-(void)getLocationsFinished:(NSArray *)array{
    _arrayOfInfos = array;
    [[self tableView] reloadData];
}
-(void)errorThrowed:(NSError *)error{
    //mostrar o erro
}
@end
