//
//  LocationManagerData.m
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "LocationManagerData.h"
#import "WebService.h"

@interface LocationManagerData()

@property (nonatomic, strong) NSArray * lista;

@end

@implementation LocationManagerData

-(void)getLocations{
    //chamar a requisicao aqui
    
     [[self delegate] getLocationsFinished:self.lista];
     [WebService getLocationsSpecificType:0 withBlock:^(ServiceResult *result) {
            //colocar aqui o código para receber a resposta do backend
            //se der erro chamar o delegate com o error throwed
            //se nao der erro chamar o delegate abaixo
        NSMutableArray *array = [NSMutableArray array];
        array = [result.resultDictionary objectForKey:@"locais"];
        NSArray * objectArray = [Local localListWithJSON:array];
        self.lista = objectArray;
        [[self delegate] getLocationsFinished:objectArray];

    }];
}

-(NSArray *)getLocationsSpecifiedType:(IUPlaceType)type{
    if (self.lista) {
        return [self filteredArray:type];
    } else {
        [self getLocations];
        return nil;
    }

}

-(NSArray *)filteredArray:(IUPlaceType)type{
    NSMutableArray * mArray = [[NSMutableArray alloc] init];
    
    for (Local * l in self.lista) {
        if (l.placeType == type) {
            [mArray addObject:l];
        }
    }
    
    return [NSArray arrayWithArray:mArray];
}

@end
