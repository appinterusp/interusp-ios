//
//  Torcida.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "Torcida.h"

@implementation Torcida

+(NSArray*)torcidasFromDictionary:(NSDictionary*)dict
{
    NSMutableArray *aux = [[NSMutableArray alloc] init];
    NSArray *values = dict[@"tracking"];
    
    for (NSDictionary *d in values) {
        Torcida *t = [[Torcida alloc] init];
        t.faculID = [d[@"facul_id"] intValue];
        t.fanCount = [d[@"users_count"] intValue];
        
        [aux addObject:t];
    }
    
    return [aux copy];
}

@end


