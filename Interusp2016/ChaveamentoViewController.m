//
//  ChaveamentoViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ChaveamentoViewController.h"
#import "ChaveamentoDetailViewController.h"
#import "SimpleCell.h"
#import "Modalidade.h"

@interface ChaveamentoViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray * listaModalidades;
@property (nonatomic, weak) IBOutlet UITableView * table;

@end

@implementation ChaveamentoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.listaModalidades = [Modalidade modalidadesChave];
    
    self.title = @"Chaveamento";
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.listaModalidades.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Modalidade * modModel = [self.listaModalidades objectAtIndex:indexPath.row];
    
    SimpleCell * cell = [self.table dequeueReusableCellWithIdentifier:@"MODALIDADECELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTitle:modModel.nome];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Modalidade * model = [self.listaModalidades objectAtIndex:indexPath.row];
    
    ChaveamentoDetailViewController * vc = [ChaveamentoDetailViewController initWithModalidade:model.nome andCode:model.modalidadeID];
    [self.navigationController pushViewController:vc animated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
