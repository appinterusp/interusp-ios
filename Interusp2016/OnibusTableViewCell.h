//
//  OnibusTableViewCell.h
//  Interusp2016
//
//  Created by Luciano Naganawa on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Onibus.h"
@interface OnibusTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *placeLbl;
@property (weak, nonatomic) IBOutlet UILabel *descricaoLbl;
-(void)confWithOnibus:(Onibus *)onibus;

@end
