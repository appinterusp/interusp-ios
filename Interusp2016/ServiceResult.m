//
//  ServiceResult.m
//  Swap
//
//  Created by AppSimples on 3/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ServiceResult.h"
@implementation ServiceResult


+(ServiceResult*)initWithStatus:(NSNumber*)statusCode message:(NSString*)message andResult:(NSDictionary*)resultDictionary{
    ServiceResult * instance = [[ServiceResult alloc] init];
    instance.resultDictionary = resultDictionary;
    instance.message = message;
    
    instance.succeeded = [statusCode intValue] == 200;
    
    instance.error = instance.succeeded?nil:[NSError errorWithDomain:@"com.appsimples" code:[statusCode integerValue] userInfo:nil];
    
    return instance;
}
+(ServiceResult*)initWithJSon:(NSDictionary*)resultDictionary{
    
    ServiceResult * instance = [[ServiceResult alloc] init];
    instance.resultDictionary = [resultDictionary objectForKey:@"response"];
    if (!instance.resultDictionary) {
        instance.resultDictionary = [resultDictionary objectForKey:@"resposta"];
    }
    int x =[[resultDictionary objectForKey:@"success"] intValue];
    instance.succeeded = x;
    instance.message = [resultDictionary objectForKey:@"message"];
    instance.error = instance.succeeded?0:[NSError errorWithDomain:instance.message code:[[resultDictionary objectForKey:@"status"] integerValue] userInfo:nil];
    return instance;
}

@end
