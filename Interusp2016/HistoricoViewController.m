//
//  HistoricoViewController.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/4/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "HistoricoViewController.h"
#import "UserConfig.h"
#import "SimpleCell.h"
#import "HistoricoDetailViewController.h"

@interface HistoricoViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, weak) IBOutlet UIButton *btnCampeosGerais;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) NSArray *arrayModalidades;
@property (nonatomic, strong) NSArray *arrayModalidadesResults;

@end

@implementation HistoricoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setArray];
    
    self.table.dataSource = self;
    self.table.delegate = self;
    
    [self.btnCampeosGerais setTitleColor:[UserConfig shared].textColor forState:UIControlStateNormal];
    [self.btnCampeosGerais setBackgroundColor:[UserConfig shared].mainColor];
    [self.lblTitle setTextColor:[UserConfig shared].mainColor];
    
    // Register a reusable cell for the table view
//    UINib *nib = [UINib nibWithNibName:@"SimpleCell" bundle:nil];
//    [self.table registerNib:nib forCellReuseIdentifier:@"SimpleCell"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setArray
{
    self.arrayModalidades = [NSArray arrayWithObjects:
                             @"Atletismo Feminino",
                             @"Atletismo Masculino",
                             @"Beisebol",
                             @"Basquete Feminino",
                             @"Basquete Masculino",
                             @"Futebol de Campo",
                             @"Futsal Feminino",
                             @"Futsal Masculino",
                             @"Handebol Feminino",
                             @"Handebol Masculino",
                             @"Judô",
                             @"Karatê Kata",
                             @"Karatê Kumite",
                             @"Natação Feminino",
                             @"Natação Masculino",
                             @"Polo Aquático",
                             @"Rugby Feminino",
                             @"Rugby Masculino",
                             @"Softbol",
                             @"Tênis de Campo Feminino",
                             @"Tênis de Campo Masculino",
                             @"Tênis de Mesa Feminino",
                             @"Tênis de Mesa Masculino",
                             @"Voleibol Feminino",
                             @"Voleibol Masculino",
                             @"Xadrez",
                             nil];
    
    self.arrayModalidadesResults = [NSArray arrayWithObjects:
                             @[@"2015: POLI", @"2014: RIBEIRAO", @"2013: PINHEIROS"],
                             @[@"2015: FEA", @"2014: FEA", @"2013: POLI"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: PINHEIROS"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: POLI"],
                             @[@"2015: FEA", @"2014: FEA", @"2013: POLI"],
                             @[@"2015: RIBEIRAO", @"2014: RIBEIRAO", @"2013: ESALQ"],
                             @[@"2015: PINHEIROS", @"2014: POLI", @"2013: PINHEIROS"],
                             @[@"2015: RIBEIRAO", @"2014: FEA", @"2013: POLI"],
                             @[@"2015: FEA", @"2014: FEA", @"2013: POLI"],
                             @[@"2015: POLI", @"2014: FEA", @"2013: PINHEIROS"],
                             @[@"2015: PINHEIROS", @"2014: POLI", @"2013: PINHEIROS"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: PINHEIROS"],
                             @[@"2015: PINHEIROS", @"2014: POLI", @"2013: PINHEIROS"],
                             @[@"2015: PINHEIROS", @"2014: POLI", @"2013: POLI"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: POLI"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: POLI"],
                             @[@"2015: FEA", @"2014: -", @"2013: -", @"OBS: As competições foram iniciadas em 2015"],
                             @[@"2015: PINHEIROS", @"2014: POLI", @"2013: FEA"],
                             @[@"2015: PINHEIROS", @"2014: FEA", @"2013: PINHEIROS"],
                             @[@"2015: PINHEIROS", @"2014: FEA", @"2013: PINHEIROS"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: PINHEIROS"],
                             @[@"2015: PINHEIROS", @"2014: POLI", @"2013: PINHEIROS"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: SANFRAN"],
                             @[@"2015: POLI", @"2014: SANFRAN", @"2013: POLI"],
                             @[@"2015: POLI", @"2014: POLI", @"2013: FEA"],
                             @[@"2015: FEA", @"2014: POLI", @"2013: FEA"],
                             nil];

}

#pragma mark - TableViewDatasource & Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrayModalidades.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.arrayModalidades objectAtIndex:indexPath.row];
    
    SimpleCell * cell = (SimpleCell*)[self.table dequeueReusableCellWithIdentifier:@"SimpleCell"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTitle:viewString];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * modalidade = [self.arrayModalidades objectAtIndex:indexPath.row];
    NSArray * results = [self.arrayModalidadesResults objectAtIndex:indexPath.row];
    
    HistoricoDetailViewController * vc = [[HistoricoDetailViewController alloc] initWithNibName:@"HistoricoDetailViewController" bundle:nil];
    vc.modalidade = modalidade;
    vc.resultados = results;
    vc.title = @"Campeões";
    [self.navigationController pushViewController:vc animated:true];
    
}

-(IBAction)campeoesGerais:(id)sender
{
    HistoricoDetailViewController * vc = [[HistoricoDetailViewController alloc] initWithNibName:@"HistoricoDetailViewController" bundle:nil];
    vc.modalidade = @"Campeões Gerais";
    vc.resultados = @[@"2015: PINHEIROS",@"2014: POLI",@"2013: PINHEIROS",@"2012: PINHEIROS",@"2011: POLI",@"2010: PINHEIROS",@"2009: POLI",@"2008: POLI",@"2007: PINHEIROS",@"2006: POLI"];
    vc.title = @"Campeões";
    [self.navigationController pushViewController:vc animated:true];
}

@end
