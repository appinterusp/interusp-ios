//
//  ViewController.m
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ViewController.h"
#import "TeamSelectionViewController.h"
#import "MapViewController.h"
#import "InformacoesViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)loadMap:(id)sender{
    MapViewController * vc = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    [self presentViewController:vc animated:true completion:nil];
}
-(IBAction)loadTeamSelection:(id)sender{
    TeamSelectionViewController * vc = [[TeamSelectionViewController alloc] initWithNibName:@"TeamSelectionViewController" bundle:nil];
    [self presentViewController:vc animated:true completion:^{}];
}
-(IBAction)loadInformacoes:(id)sender{
    InformacoesViewController * vc = [[InformacoesViewController alloc] initWithNibName:@"InformacoesViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:navController animated:true completion:nil];
}
@end
