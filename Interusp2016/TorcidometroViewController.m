//
//  TorcidometroViewController.m
//  Interusp2016
//
//  Created by Luciano Naganawa on 5/4/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "TorcidometroViewController.h"
#import "WebService.h"
#import "Torcida.h"
#import "TorcidaCell.h"

@interface TorcidometroViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *torcidas;
@property int fanCount;
@property int max;
@property (nonatomic, strong) IBOutlet UITableView * table;
@property (nonatomic, strong) IBOutlet UILabel * total;

@end

@implementation TorcidometroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.table.delegate = self;
    self.table.dataSource = self;
    self.fanCount = 0;
    self.max = 0;
    
    [WebService getTrackingWithBlock:^(ServiceResult *result) {
        self.torcidas = [Torcida torcidasFromDictionary:result.resultDictionary];
        
        for (Torcida *t in self.torcidas) {
            self.fanCount = self.fanCount + t.fanCount;
            if (t.fanCount > self.max) {
                self.max = t.fanCount;
            }
        }
        
        self.total.text = [NSString stringWithFormat:@"Total: %d", self.fanCount];
        [self.table reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.torcidas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Torcida * torcidaModel = [self.torcidas objectAtIndex:indexPath.row];
    
    TorcidaCell * cell = [self.table dequeueReusableCellWithIdentifier:@"TORCIDACELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"TorcidaCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithModel:torcidaModel andTotal:self.max];
    return cell;
}

@end
