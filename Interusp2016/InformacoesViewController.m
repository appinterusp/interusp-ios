//
//  InformacoesViewController.m
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "InformacoesViewController.h"
#import "InfoEspViewController.h"
#import "LocationManagerData.h"
#import "OnibusViewController.h"
@interface InformacoesViewController ()<LocationManagerDataDelegate> {
    LocationManagerData *locData;
}

@property (nonatomic, strong) IBOutlet UIView * container;

@end

@implementation InformacoesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Informações";
    
    self.container.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    locData = [[LocationManagerData alloc] init];
    locData.delegate = self;
    
    [locData getLocations];
}
- (IBAction)selected:(UIButton *)sender {
    
    if (sender.tag == 4) {
        OnibusViewController *onibus = [[OnibusViewController alloc] initWithNibName:@"OnibusViewController" bundle:nil];
        [[self navigationController] pushViewController:onibus animated:true];

        return;
    }
    
    NSArray * filterArray = [locData getLocationsSpecifiedType:sender.tag];
    
    if (filterArray) {
        InfoEspViewController *infoEsp = [[InfoEspViewController alloc] init];
        infoEsp.arrayOfElements = filterArray;
        [[self navigationController] pushViewController:infoEsp animated:true];
    }
    
    
}

-(void)getLocationsFinished:(NSArray *)array{
    //array OfLocations = array;
    //[self addPointMap];
    //[loadingView removeFromSuperview];
    //loadingView = nil;
    
}
-(void)errorThrowed:(NSError *)error{
    //[loadingView removeFromSuperview];
    //loadingView = nil;
}


@end
