//
//  ChaveView.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ChaveView.h"

@interface ChaveView ()

@property (nonatomic, weak) IBOutlet UILabel * lblTeam1, * lblTeam2,* lblTeam3, * lblTeam4,* lblTeam5, * lblTeam6,* lblTeam7, * lblTeam8,* lblTeam9, * lblTeam10,* lblTeam11, * lblTeam12,* lblTeam13, * lblTeam14, * lblTeam15;

@end

@implementation ChaveView

-(void)parseToView:(NSArray*)models{
    for (NSDictionary * dict in models) {
        UILabel * lbl1;
        UILabel * lbl2;
        
        if ([[dict objectForKey:@"chaveamento"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            lbl1 = self.lblTeam8;
            lbl2 = self.lblTeam9;
        }
        if ([[dict objectForKey:@"chaveamento"] isEqualToNumber:[NSNumber numberWithInt:2]]) {
            lbl1 = self.lblTeam10;
            lbl2 = self.lblTeam11;
        }
        if ([[dict objectForKey:@"chaveamento"] isEqualToNumber:[NSNumber numberWithInt:3]]) {
            lbl1 = self.lblTeam12;
            lbl2 = self.lblTeam13;
        }
        if ([[dict objectForKey:@"chaveamento"] isEqualToNumber:[NSNumber numberWithInt:4]]) {
            lbl1 = self.lblTeam14;
            lbl2 = self.lblTeam15;
        }
        if ([[dict objectForKey:@"chaveamento"] isEqualToNumber:[NSNumber numberWithInt:5]]) {
            lbl1 = self.lblTeam4;
            lbl2 = self.lblTeam5;
        }
        if ([[dict objectForKey:@"chaveamento"] isEqualToNumber:[NSNumber numberWithInt:6]]) {
            lbl1 = self.lblTeam6;
            lbl2 = self.lblTeam7;
        }
        if ([[dict objectForKey:@"chaveamento"] isEqualToNumber:[NSNumber numberWithInt:7]]) {
            lbl1 = self.lblTeam2;
            lbl2 = self.lblTeam3;
            //self.lblTeam2.text = [ChaveView nomeForFaculdade:[[dict objectForKey:@"faculdade_1"] intValue]];
            //self.lblTeam3.text = [ChaveView nomeForFaculdade:[[dict objectForKey:@"faculdade_2"] intValue]];
        }
        if ([[dict objectForKey:@"is_vencedor"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            self.lblTeam1.text = [ChaveView nomeForFaculdade:[[dict objectForKey:@"faculdade_1"] intValue]];

        } else {
            NSString * s1 = [ChaveView nomeForFaculdade:[[dict objectForKey:@"faculdade_1"] intValue]];
            NSNumber * n1 = [dict objectForKey:@"placar_1"];
            if (n1) {
                s1 = [s1 stringByAppendingFormat:@" - %d", [n1 intValue]];
            }
            lbl1.text = s1;
            
            NSString * s2 = [ChaveView nomeForFaculdade:[[dict objectForKey:@"faculdade_2"] intValue]];
            NSNumber * n2 = [dict objectForKey:@"placar_2"];
            if (n2) {
                s2 = [s2 stringByAppendingFormat:@" - %d", [n2 intValue]];
            }
            lbl2.text = s2;
            
            lbl1.backgroundColor = [UIColor clearColor];
            lbl2.backgroundColor = [UIColor clearColor];
            
//            if ([dict objectForKey:@"mandante"]) {
//                if ([[dict objectForKey:@"mandante"] intValue]==1) {
//                    lbl1.backgroundColor = [UIColor yellowColor];
//                }
//                if ([[dict objectForKey:@"mandante"] intValue]==2) {
//                    lbl2.backgroundColor = [UIColor yellowColor];
//                }
//            }
        }
    }
}

+(NSString*)nomeForFaculdade:(int)code{
    NSString * name = @"";
    switch (code) {
        case 1:
            name = @"POLI";
            break;
            
        case 2:
            name = @"FEA";
            break;
            
        case 3:
            name = @"FARMA";
            break;
            
        case 4:
            name = @"ESALQ";
            break;
            
        case 5:
            name = @"RIBEIRÃO";
            break;
            
        case 6:
            name = @"SANFRAN";
            break;
            
        case 7:
            name = @"ODONTO";
            break;
            
        case 8:
            name = @"PINHEIROS";
            break;
            
        default:
            break;
    }
    return name;
}


@end
