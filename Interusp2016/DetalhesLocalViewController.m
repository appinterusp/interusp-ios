//
//  DetalhesLocalViewController.m
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "DetalhesLocalViewController.h"
#import "WebService.h"
#import "JogosViewController.h"

@interface DetalhesLocalViewController ()

@property(nonatomic, weak) IBOutlet UILabel *lblNome;
//@property(nonatomic, weak) IBOutlet UILabel *lblInformacoes;
//@property(nonatomic, weak) IBOutlet UILabel *lblEndereco;
@property(nonatomic, weak) IBOutlet UIImageView * bigImage;
@property(nonatomic, weak) IBOutlet UITextView *txtView;

@property(nonatomic, weak) IBOutlet UIButton *btnMapa;
@property(nonatomic, weak) IBOutlet UIButton *btnJogos;

@end

@implementation DetalhesLocalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblNome.text = self.local.name;
    //self.lblInformacoes.text = @"";
    self.txtView.text = self.local.endereco;
    self.txtView.scrollEnabled = NO;
    self.txtView.scrollEnabled = YES;
    
    [self.btnMapa setTitleColor:[UserConfig shared].textColor forState:UIControlStateNormal];
    [self.btnMapa setBackgroundColor:[UserConfig shared].mainColor];
    
    [self.btnJogos setTitleColor:[UserConfig shared].textColor forState:UIControlStateNormal];
    [self.btnJogos setBackgroundColor:[UserConfig shared].mainColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    if (self.local.imageURL) {
        if (self.local.image) {
            self.bigImage.image = self.local.image;
        } else {
            [WebService getPhotosFromURL:self.local.imageURL withBlock:^(UIImage *result, NSError *error) {
                if (result) {
                    self.bigImage.image = result;
                    self.bigImage.clipsToBounds = YES;
                }
            }];
        }
        
    }
}

#pragma mark - BUTTONS

-(IBAction)openGmaps:(id)sender
{
    //Google Maps
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"comgooglemaps://?q=%f,%f", self.local.latitude, self.local.longitude]];
    
    if (![[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]) {
        NSLog(@"Google Maps app is not installed");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/en/app/google-maps/id585027354?mt=8"]];
        //TRATAR ERRO DO GOOGLE
    } else {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(IBAction)jogos:(id)sender
{
    UINavigationController *nav = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:3];
    JogosViewController *vc = (JogosViewController *) nav.viewControllers[0];
    [vc openLocal];
    [self.tabBarController setSelectedIndex:3];
    
}

@end
