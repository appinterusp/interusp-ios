//
//  GritosViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/3/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "GritosViewController.h"
#import "SimpleCell.h"
#import "GritoDetailViewController.h"

@interface GritosViewController ()

@property (nonatomic, strong) NSArray * lista;
@property (nonatomic, weak) IBOutlet UITableView * table;

@end

@implementation GritosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.lista = @[@"POLI",@"FEA",@"ODONTO", @"FARMA", @"ESALQ"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lista.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.lista objectAtIndex:indexPath.row];
    
    SimpleCell * cell = [self.table dequeueReusableCellWithIdentifier:@"SIMPLECELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTitle:viewString];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.lista objectAtIndex:indexPath.row];
    
    GritoDetailViewController * vc = [[GritoDetailViewController alloc] initWithNibName:@"GritoDetailViewController" bundle:nil];
    vc.faculdade = viewString;
    vc.title = [@"Gritos " stringByAppendingString:viewString];
    [self.navigationController pushViewController:vc animated:true];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
