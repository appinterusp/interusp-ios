//
//  LoadingView.h
//  Swap
//
//  Created by AppSimples on 3/18/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

+(LoadingView*)addLoadingViewToView:(UIView*)parentView;

@end
