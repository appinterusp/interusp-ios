//
//  ScoreTeamViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ScoreTeamViewController.h"
#import "UserConfig.h"
#import "ScoreTeamCell.h"
#import "ScoreTeamHeader.h"
#import "WebService.h"

@interface ScoreTeamViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView * table;
@property (nonatomic, weak) IBOutlet UILabel * atletica;
@property (nonatomic, strong) NSArray * lista;
@property (nonatomic, strong) NSArray * modalidades;
@property (nonatomic, strong) NSArray * faculdades;

@end

@implementation ScoreTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.faculdades = [NSArray arrayWithObjects:@"POLI",@"FEA", @"FARMA",@"ESALQ",@"RIBEIRÃO",@"SANFRAN",@"ODONTO", @"PINHEIROS",nil];
    
    self.atletica.text = [self.faculdades objectAtIndex:(self.faculdadeId-1)];
    self.atletica.textColor = [UserConfig shared].mainColor;
    
    self.table.dataSource = self;
    self.table.delegate = self;
    
    self.modalidades = [NSArray arrayWithObjects:
                        @"corridaF.png",
                        @"corridahdpi.png",
                        @"basqueteF.png",
                        @"basquetehdpi.png",
                        @"softhdpi.png",
                        @"campohdpi.png",
                        @"futsalF.png",
                        @"futsalhdpi.png",
                        @"handballF.png",
                        @"handballhdpi.png",
                        @"judohdpi.png",
                        @"karatehdpi.png",
                        @"natacaoF.png",
                        @"natacaohdpi.png",
                        @"polohdpi.png",
                        @"rugbyhdpi.png",
                        @"softF.png",
                        @"tenisF.png",
                        @"tenishdpi.png",
                        @"tenismesaF.png",
                        @"tenismesahdpi.png",
                        @"voleiF.png",
                        @"voleixhdpi.png",
                        @"xadrezhdpi.png",
                        @"rugbyF.png",
                      nil];
    
    [WebService getPontuaçãoFaculdade:self.faculdadeId WithBlock:^(ServiceResult *result) {
        if (result.succeeded) {
            NSArray * a = [result.resultDictionary objectForKey:@"modalidades"];
            if (a && a.count>0) {
                self.lista = a;
                [self.table reloadData];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lista.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * model = [self.lista objectAtIndex:indexPath.row];
    
    int index = [[model objectForKey:@"id"] intValue]-1;
    NSString *posicao = [[model objectForKey:@"posicao"] stringValue];
    NSString *pontuacao = [[model objectForKey:@"pontuacao_total"] stringValue];
    UIImage *foto = [UIImage imageNamed:[self.modalidades objectAtIndex:index]];
    
    ScoreTeamCell * cell = [self.table dequeueReusableCellWithIdentifier:@"SCORECELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"ScoreTeamCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithSport:foto score:pontuacao position:posicao];
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ScoreTeamHeader" owner:nil options:nil];
    if ([arrayOfViews count] < 1) return nil;
    ScoreTeamHeader *header = (ScoreTeamHeader*)[arrayOfViews objectAtIndex:0];
    
    return header;
}


@end
