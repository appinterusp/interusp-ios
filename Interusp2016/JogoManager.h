//
//  JogoManager.h
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JogoManagerDelegate <NSObject>

@required

-(void)jogoManagerDidUpdateGames:(NSArray *)gameList;
-(void)jogoManagerError:(NSString *)errorString;

@end

@interface JogoManager : NSObject

-(void)getJogos;
-(void)configFilter:(NSString*)filter withValue:(NSString*)value;

@property (nonatomic, weak) id<JogoManagerDelegate> delegate;

@end
