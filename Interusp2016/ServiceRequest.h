//
//  ServiceRequest.h
//  Swap
//
//  Created by AppSimples on 3/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceResult.h"

typedef void (^ASServiceBlock)(ServiceResult * result);

@interface ServiceRequest : NSObject

+(void)dispatchService:(NSMutableURLRequest*)serviceRequest withServiceBlock:(ASServiceBlock)serviceblock;

//Service Helpers
+(NSMutableURLRequest*)requestWithURL:(NSString*)urlString method:(NSString*)method andBody:(NSDictionary*)body withToken:(NSString*)token;
+(NSString *)jsonStringWithDictionary:(NSDictionary*)dict;

@end
