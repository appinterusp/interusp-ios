//
//  MainTabController.m
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "MainTabController.h"
#import "JogosViewController.h"
#import "UserConfig.h"
#import "ChaveamentoViewController.h"
#import "ScoreGeralViewController.h"
#import "MapViewController.h"
#import "InformacoesViewController.h"
#import "MoreViewController.h"

@interface MainTabController ()

@end

@implementation MainTabController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(MainTabController *)initMainTabs{
    MainTabController * instance = [[MainTabController alloc] init];
    instance.tabBar.tintColor = [UserConfig shared].textColor;
    instance.tabBar.barTintColor = [UserConfig shared].mainColor;
    instance.tabBar.translucent = false;
    
    JogosViewController * jogosView = [[JogosViewController alloc] initWithNibName:@"JogosViewController" bundle:nil];
    jogosView.title = @"Jogos";
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStylePlain target:jogosView action:@selector(refresh)];
    jogosView.navigationItem.rightBarButtonItem = anotherButton;
    UINavigationController * jogosNav = [[UINavigationController alloc] initWithRootViewController:jogosView];
    jogosNav.navigationBar.translucent = false;
    jogosNav.navigationBar.tintColor = [UserConfig shared].textColor;
    jogosNav.navigationBar.barTintColor = [UserConfig shared].mainColor;
    jogosNav.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [jogosNav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UserConfig shared].textColor}];
    UIImage *itemJogo = [UIImage imageNamed:@"tabicon_branco_jogos"];
    jogosNav.tabBarItem.image = [[instance imageWithImage:itemJogo scaledToSize:CGSizeMake(35, 35) andAlpha:0.5] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    jogosNav.tabBarItem.selectedImage = [[instance imageWithImage:itemJogo scaledToSize:CGSizeMake(35, 35) andAlpha:1] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    ChaveamentoViewController * chaveView = [[ChaveamentoViewController alloc] initWithNibName:@"ChaveamentoViewController" bundle:nil];
    chaveView.title = @"Chaveamento";
    UINavigationController * chaveNav = [[UINavigationController alloc] initWithRootViewController:chaveView];
    chaveNav.navigationBar.translucent = false;
    chaveNav.navigationBar.tintColor = [UserConfig shared].textColor;
    chaveNav.navigationBar.barTintColor = [UserConfig shared].mainColor;
    chaveNav.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [chaveNav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UserConfig shared].textColor}];
    UIImage *itemChave = [UIImage imageNamed:@"tabicon_branco_chave"];
    chaveNav.tabBarItem.image = [[instance imageWithImage:itemChave scaledToSize:CGSizeMake(35, 35) andAlpha:0.5] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    chaveNav.tabBarItem.selectedImage = [[instance imageWithImage:itemChave scaledToSize:CGSizeMake(35, 35) andAlpha:1] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    /*ScoreGeralViewController * scoreView = [[ScoreGeralViewController alloc] initWithNibName:@"ScoreGeralViewController" bundle:nil];
    scoreView.title = @"INFORMAÇÃO";
    UINavigationController * scoreNav = [[UINavigationController alloc] initWithRootViewController:scoreView];
    scoreNav.navigationBar.translucent = false;
    scoreNav.navigationBar.tintColor = [UserConfig shared].textColor;
    scoreNav.navigationBar.barTintColor = [UserConfig shared].mainColor;
    scoreNav.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [scoreNav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UserConfig shared].textColor}];
    UIImage *itemScore = [UIImage imageNamed:@"tabicon_branco_info"];
    scoreNav.tabBarItem.image = [[instance imageWithImage:itemScore scaledToSize:CGSizeMake(35, 35) andAlpha:0.5] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    scoreNav.tabBarItem.selectedImage = [[instance imageWithImage:itemScore scaledToSize:CGSizeMake(35, 35) andAlpha:1] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];*/
    
    InformacoesViewController * infoView = [[InformacoesViewController alloc] init];
    UINavigationController * infoNav = [[UINavigationController alloc] initWithRootViewController:infoView];
    infoNav.navigationBar.translucent = false;
    infoNav.navigationBar.tintColor = [UserConfig shared].textColor;
    infoNav.navigationBar.barTintColor = [UserConfig shared].mainColor;
    infoNav.navigationBar.barStyle = UIBarStyleBlackOpaque;
    infoNav.title = @"Informações";
    [infoNav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UserConfig shared].textColor}];
    UIImage *itemScore = [UIImage imageNamed:@"tabicon_branco_info"];
    infoNav.tabBarItem.image = [[instance imageWithImage:itemScore scaledToSize:CGSizeMake(35, 35) andAlpha:0.5] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    infoNav.tabBarItem.selectedImage = [[instance imageWithImage:itemScore scaledToSize:CGSizeMake(35, 35) andAlpha:1] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    MapViewController * mapView = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    mapView.title = @"Mapa";
    UINavigationController * mapNav = [[UINavigationController alloc] initWithRootViewController:mapView];
    mapNav.navigationBar.translucent = false;
    mapNav.navigationBar.tintColor = [UserConfig shared].textColor;
    mapNav.navigationBar.barTintColor = [UserConfig shared].mainColor;
    mapNav.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [mapNav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UserConfig shared].textColor}];
    UIImage *itemMapa = [UIImage imageNamed:@"tabicon_branco_mapa"];
    mapNav.tabBarItem.image = [[instance imageWithImage:itemMapa scaledToSize:CGSizeMake(35, 35) andAlpha:0.5] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    mapNav.tabBarItem.selectedImage = [[instance imageWithImage:itemMapa scaledToSize:CGSizeMake(35, 35) andAlpha:1] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    MoreViewController * moreView = [[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
    moreView.title = @"Mais";
    UINavigationController * moreNav = [[UINavigationController alloc] initWithRootViewController:moreView];
    moreNav.navigationBar.translucent = false;
    moreNav.navigationBar.tintColor = [UserConfig shared].textColor;
    moreNav.navigationBar.barTintColor = [UserConfig shared].mainColor;
    moreNav.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [moreNav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UserConfig shared].textColor}];
    UIImage *itemMore = [UIImage imageNamed:@"tabicon_branco_mais"];
    moreNav.tabBarItem.image = [[instance imageWithImage:itemMore scaledToSize:CGSizeMake(35, 35) andAlpha:0.5] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    moreNav.tabBarItem.selectedImage = [[instance imageWithImage:itemMore scaledToSize:CGSizeMake(35, 35) andAlpha:1] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    instance.viewControllers = @[infoNav, chaveNav, mapNav, jogosNav, moreNav];
    [instance setSelectedIndex:2];
    
    return instance;
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize andAlpha:(CGFloat)alpha {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetAlpha(ctx, alpha);
    //[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    CGRect area = CGRectMake(0, 0, newSize.width, newSize.height);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    CGContextDrawImage(ctx, area, [image CGImage]);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
