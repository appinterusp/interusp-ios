//
//  GritoDetailViewController.m
//  Interusp2016
//
//  Created by AppSimples on 5/3/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "GritoDetailViewController.h"
#import "SimpleCell.h"
#import "GritoLetraViewController.h"

@interface GritoDetailViewController ()

@property (nonatomic, strong) NSArray * lista;
@property (nonatomic, weak) IBOutlet UITableView * table;

@end

@implementation GritoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.lista = [[NSArray alloc] init];
    
    if ([self.faculdade isEqualToString:@"POLI"]) {
        self.lista = @[@"Guerreiro",@"Poli Maravilha",@"Horto Mágico",@"Bata Amarela"];
    }
    if ([self.faculdade isEqualToString:@"FEA"]) {
        self.lista = @[@"Incessante",@"Funk do Cachorro",@"Tralalalaô",@"Fea USP Maravilhosa"];
    }
    if ([self.faculdade isEqualToString:@"ESALQ"]) {
        self.lista = @[@"Hino do Agricolão"];
    }
    if ([self.faculdade isEqualToString:@"ODONTO"]) {
        self.lista = @[@"FOUSP Maravilhosa",@"Odonto Minha Paixão"];
    }
    if ([self.faculdade isEqualToString:@"FARMA"]) {
        self.lista = @[@"Ami"];
    }
    if ([self.faculdade isEqualToString:@"PINHEIROS"]) {
        self.lista = @[@"Grito"];
    }
    if ([self.faculdade isEqualToString:@"RIBEIRÃO"]) {
        self.lista = @[@"Grito"];
    }
    if ([self.faculdade isEqualToString:@"SANFRAN"]) {
        self.lista = @[@"Grito"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDatasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lista.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.lista objectAtIndex:indexPath.row];
    
    SimpleCell * cell = [self.table dequeueReusableCellWithIdentifier:@"SIMPLECELLID"];
    if (!cell) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configWithTitle:viewString];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * viewString = [self.lista objectAtIndex:indexPath.row];
    
    GritoLetraViewController * view = [[GritoLetraViewController alloc] initWithNibName:@"GritoLetraViewController" bundle:nil];
    //view.title = viewString;
    view.grito = viewString;
    [self.navigationController pushViewController:view animated:true];
    
}

@end
