//
//  WebService.h
//  Swap
//
//  Created by AppSimples on 3/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceRequest.h"
#import "Local.h"
#import "UserConfig.h"
@interface WebService : NSObject

+(void)getLocationsSpecificType:(IUPlaceType)placeType withBlock:(ASServiceBlock)serviceBlock;
+(void)getPhotosFromURL:(NSString*)url withBlock:(void (^)(UIImage *result, NSError *error))completionBlock;
+(void)getBusByCollege: (IUTeam) college withBlock:(ASServiceBlock)serviceBlock;

+(void)getGamesForModalidade:(int)modalidadeCode withBlock:(ASServiceBlock)serviceBlock;
+(void)getPontuaçãoWithBlock:(ASServiceBlock)serviceBlock;
+(void)getPontuaçãoModalidade:(int)modalidadeCode WithBlock:(ASServiceBlock)serviceBlock;
+(void)getPontuaçãoFaculdade:(int)faculdadeCode WithBlock:(ASServiceBlock)serviceBlock;

+(void)getTrackingWithBlock:(ASServiceBlock)serviceBlock;
+(void)addTracking:(int)torcida WithBlock:(ASServiceBlock)serviceBlock;

@end
