//
//  OnibusViewController.h
//  Interusp2016
//
//  Created by Luciano Naganawa on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnibusDataManager.h"

@interface OnibusViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, OnibusDataManagerDelegate>
@property NSArray *arrayOfInfos;

@end
