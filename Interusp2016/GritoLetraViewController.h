//
//  GritoLetraViewController.h
//  Interusp2016
//
//  Created by AppSimples on 5/3/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GritoLetraViewController : UIViewController

@property (nonatomic, strong) NSString * grito;

@end
