//
//  WebService.m
//  Swap
//
//  Created by AppSimples on 3/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "WebService.h"
#import "WebConstants.h"
#import "LocationHelper.h"

#define kCacheImagePath @"IMAGEPATH"

@implementation WebService
+(void)getLocationsSpecificType:(IUPlaceType)placeType withBlock:(ASServiceBlock)serviceBlock{
    NSString *str =[URL_WS stringByAppendingString:GET_PLACES_ENDPOINT];
    //NSDictionary *inventory = @{PLACE_TYPE : [LocationHelper getStringForPlaceType:placeType],};
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:str method:@"GET" andBody:nil withToken:nil];
    
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];

}
+(void)getBusByCollege: (IUTeam) college withBlock:(ASServiceBlock)serviceBlock{
    NSString * urlString = [URL_WS stringByAppendingString:ONIBUS];
    urlString = [urlString stringByAppendingFormat:@"%d", college];
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:urlString method:@"GET" andBody:nil withToken:nil];
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];
}

+(void)getGamesForModalidade:(int)modalidadeCode withBlock:(ASServiceBlock)serviceBlock{
    NSString * urlString = [URL_WS stringByAppendingString:GET_JOGOS];
    if (modalidadeCode!=0) {
        urlString = [urlString stringByAppendingFormat:@"modalidade=%d/", modalidadeCode];
    }
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:urlString method:@"GET" andBody:nil withToken:nil];
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];
}

+(void)getPontuaçãoWithBlock:(ASServiceBlock)serviceBlock{
    NSString * urlString = [URL_WS stringByAppendingString:GET_FACULDADES];
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:urlString method:@"GET" andBody:nil withToken:nil];
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];
}

+(void)getPontuaçãoModalidade:(int)modalidadeCode WithBlock:(ASServiceBlock)serviceBlock{
    //modalidade?id=1
    NSString * urlString = [URL_WS stringByAppendingFormat:@"modalidade?id=%d", modalidadeCode];
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:urlString method:@"GET" andBody:nil withToken:nil];
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];
}

+(void)getPontuaçãoFaculdade:(int)faculdadeCode WithBlock:(ASServiceBlock)serviceBlock{
    //modalidade?id=1
    NSString * urlString = [URL_WS stringByAppendingFormat:@"modalidade/faculdade=%d", faculdadeCode];
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:urlString method:@"GET" andBody:nil withToken:nil];
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];
}

+(void)getTrackingWithBlock:(ASServiceBlock)serviceBlock{
    NSString * urlString = [URL_WS stringByAppendingFormat:GET_TORCIDAS];
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:urlString method:@"GET" andBody:nil withToken:nil];
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];
}

+(void)addTracking:(int)torcida WithBlock:(ASServiceBlock)serviceBlock{
    NSString * urlString = [URL_WS stringByAppendingFormat:GET_TORCIDAS];
    NSDictionary * postBody = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:torcida],@"facul_id", nil];
    NSMutableURLRequest *request = [ServiceRequest requestWithURL:urlString method:@"POST" andBody:postBody withToken:nil];
    [ServiceRequest dispatchService:request withServiceBlock:serviceBlock];
}

+(void)getPhotosFromURL:(NSString*)url withBlock:(void (^)(UIImage *result, NSError *error))completionBlock
{
    NSString *newUrl = [url stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *folderPath = [NSHomeDirectory() stringByAppendingString:kCacheImagePath];
    NSString* path = [folderPath stringByAppendingFormat:@"/%@", [WebService fileNameFromURL:newUrl]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        UIImage *img = [UIImage imageWithContentsOfFile:path];
        completionBlock(img,nil);
    }
    else
    {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:newUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:400.0];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSError *errorResponse = nil;
            NSHTTPURLResponse *responseCode = nil;
            NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&errorResponse];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!errorResponse) {
                    [WebService saveImage:responseData withURL:newUrl];
                    UIImage *img = [UIImage imageWithData:responseData];
                    completionBlock(img,errorResponse);
                }else{
                    completionBlock(nil,errorResponse);
                }
                
            });
        });
    }
}

+ (BOOL)saveImage:(NSData*)data withURL:(NSString*)url
{
    NSString *folderPath = [NSHomeDirectory() stringByAppendingString:kCacheImagePath];
    NSFileManager *fm = [[NSFileManager alloc] init];
    if (![fm fileExistsAtPath:folderPath])
    {
        [fm createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *imagePath = [folderPath stringByAppendingFormat:@"/%@", [WebService fileNameFromURL:url]];
    BOOL save = [data writeToFile:imagePath atomically:YES];
    return save;
}

+(NSString*)fileNameFromURL:(NSString*)url
{
    NSString *nameFile = [url stringByReplacingOccurrencesOfString:@"/" withString:@""];
    nameFile = [nameFile stringByReplacingOccurrencesOfString:@":" withString:@""];
    return nameFile;
}

@end
