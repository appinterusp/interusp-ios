//
//  PontuacaoGeralTableViewCell.h
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreGeralCell : UITableViewCell

-(void)configWithTeam:(NSString*)team score:(NSString*)score minScore:(NSString*)minScore maxScore:(NSString*)maxScore;

@end
