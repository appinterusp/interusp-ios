//
//  ModalidadeCell.m
//  Interusp2016
//
//  Created by AppSimples on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "SimpleCell.h"

@interface SimpleCell ()

@property (nonatomic, weak) IBOutlet UILabel * lblTitle;

@end

@implementation SimpleCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self clean];
}

-(void)clean{
    self.lblTitle.text = nil;
}

-(void)configWithTitle:(NSString *)title{
    self.lblTitle.text = title;
}

@end
