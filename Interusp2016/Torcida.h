//
//  Torcida.h
//  Interusp2016
//
//  Created by Gabriel Oliva de Oliveira on 5/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Torcida : NSObject

@property int faculID;
@property int fanCount;

+(NSArray*)torcidasFromDictionary:(NSDictionary*)dict;

@end
