//
//  LocationHelper.h
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>
@interface LocationHelper : NSObject
typedef enum {
    IUPlaceNone = -1,
    IUPlaceNotSelected = 0,
    IUPlaceGym = 1,
    IUPlaceTendas,
    IUPlaceBalada,
    IUPlaceOnibus,
    IUPlaceAlojamento,
    IUPlaceHospital,
    IUPlaceDelegacia,
    IUPlaceRestaurantes
} IUPlaceType;

+(NSString *)getStringForPlaceType:(IUPlaceType)placeType;
+(UIImage *)getImageForPlaceType:(IUPlaceType)placeType;

@end
