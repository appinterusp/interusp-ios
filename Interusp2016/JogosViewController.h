//
//  JogosViewController.h
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JogosViewController : UIViewController

-(void)openLocal;
-(void)refresh;

@end
