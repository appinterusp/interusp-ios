//
//  ServiceRequest.m
//  Swap
//
//  Created by AppSimples on 3/17/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "ServiceRequest.h"
#import "WebConstants.h"
@implementation ServiceRequest

#pragma mark - RequestDispatcher

+(void)dispatchService:(NSMutableURLRequest*)serviceRequest withServiceBlock:(ASServiceBlock)serviceblock{
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        
        NSError *errorResponse = nil;
        NSHTTPURLResponse *responseCode = nil;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&responseCode error:&errorResponse];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *errorResult = nil;
            if (errorResponse || responseData.length == 0) {
                ServiceResult * result = [ServiceResult initWithStatus:[NSNumber numberWithInteger:NotConnectServerDown] message:errorResponse.localizedFailureReason andResult:nil];
                serviceblock(result);
                return;
            }
            NSDictionary * resultJSON = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&errorResult];
            NSLog(@"%@", resultJSON);
            ServiceResult *res = [ServiceResult initWithJSon:resultJSON];
            serviceblock(res);
        });
    });
}

#pragma mark - Helpers

+(NSMutableURLRequest*)requestWithURL:(NSString*)urlString method:(NSString*)method andBody:(NSDictionary*)body withToken:(NSString*)token{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];

    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:token forHTTPHeaderField:@"x-access-token"]; //4
    
    [request setHTTPMethod:method];
    if (body) {
        NSData* data = [NSJSONSerialization dataWithJSONObject:body options:0 error:NULL]; //3

        [request setHTTPBody:data];
    }
    return request;
}

+(NSString *)jsonStringWithDictionary:(NSDictionary*)dict{
    NSError *error;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

@end
