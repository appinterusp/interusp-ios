//
//  OnibusDataManager.h
//  Interusp2016
//
//  Created by Luciano Naganawa on 5/5/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol OnibusDataManagerDelegate
@optional
-(void)getLocationsFinished:(NSArray *)array;
-(void)errorThrowed:(NSError *)error;
@end
@interface OnibusDataManager : NSObject
-(void)getOnibus;
@property id <OnibusDataManagerDelegate> delegate;
@end
