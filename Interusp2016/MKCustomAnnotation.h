//
//  MKCustomAnnotation.h
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Local.h"


@interface MKCustomAnnotation : MKAnnotationView
@property Local *loc;
@end
