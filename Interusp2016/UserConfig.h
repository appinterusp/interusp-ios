//
//  UserConfig.h
//  Interusp2016
//
//  Created by AppSimples on 4/30/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    IUTeamNone = -1,
    IUTeamNotSelected = 0,
    IUTeamPoli = 1,
    IUTeamFea,
    IUTeamFarma,
    IUTeamESALQ,
    IUTeamRibeirao,
    IUTeamSanFran,
    IUTeamOdonto,
    IUTeamPinheiros
} IUTeam;

@interface UserConfig : NSObject

@property IUTeam selectedTeam;
@property NSString *selectedTeamName;

@property (nonatomic, strong) UIColor * mainColor, *textColor;

-(void)selectTeam:(IUTeam)team;

+(UserConfig*)shared;

@end
