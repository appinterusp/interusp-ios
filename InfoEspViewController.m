//
//  InfoEspViewController.m
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import "InfoEspViewController.h"
#import "LocationHelper.h"
#import "LocationManagerData.h"
#import "LocaisViewController.h"
#import "LoadingView.h"
#import "DetalhesLocalViewController.h"


@interface InfoEspViewController () <UITableViewDelegate, UITableViewDataSource, LocationManagerDataDelegate>{
    
    LocationManagerData *locationDataManager;
    LoadingView *loadingView;
}
@property  UITableView *tableView;

@end

@implementation InfoEspViewController
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *reuse = @"reuseCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuse];
    if (!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuse];
    }
    Local *loc = [self.arrayOfElements objectAtIndex:indexPath.row];

    [[cell imageView] setImage:loc.image];
    [[cell textLabel] setText:loc.name];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrayOfElements count];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    self.title = [LocationHelper getStringForPlaceType:[self type]];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Local *loc = [self.arrayOfElements objectAtIndex:indexPath.row];
    
    DetalhesLocalViewController * view = [[DetalhesLocalViewController alloc] initWithNibName:@"DetalhesLocalViewController" bundle:nil];
    view.local = loc;
    [self.navigationController pushViewController:view animated:true];
    
}

@end
