//
//  LocaisViewController.h
//  Interusp2016
//
//  Created by Joao Sisanoski on 5/1/16.
//  Copyright © 2016 AppSimples. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Local.h"
@interface LocaisViewController : UIViewController
@property Local *local;
@end
